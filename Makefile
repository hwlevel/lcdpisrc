KERNEL_GE_4_1 := $(shell [ $(VERSION) -ge 4 -a $(PATCHLEVEL) -ge 1 ] && echo true)
ifeq ($(KERNEL_GE_4_1),true)
	obj-$(CONFIG_LCDPI_SPI_DMA)        += spi-bcm2835_dma.o
else
	obj-$(CONFIG_LCDPI_SPI_DMA)        += spi-bcm2708_dma.o
endif

ifeq ($(CONFIG_LOCALVERSION),"-v7")
        obj-$(CONFIG_LCDPI_COMPRESS)	   += compress_v7.o
else
        obj-$(CONFIG_LCDPI_COMPRESS)	   += compress_v6.o
endif

obj-$(CONFIG_LCDPI_GSLX680)	       += i2c-bcm2708.o
obj-$(CONFIG_FB_SSD1963)           += ssd1963.o
obj-$(CONFIG_FB_PI40)              += ili948x.o
obj-$(CONFIG_FB_KD043)             += hx8369.o
obj-$(CONFIG_FB_D51)               += d51eta5701.o
obj-$(CONFIG_FB_SSD1289)           += ssd1289.o
obj-$(CONFIG_LCDPI_ADS7846)        += lcdpitp.o
obj-$(CONFIG_LCDPI_GSLX680)        += gslx680.o
obj-$(CONFIG_CBSR_HT16K33)         += ht16k33.o
