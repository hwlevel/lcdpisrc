/*
 * Framebuffer driver for 480x320 Hwlevel display modules
 * based on ILI9486 / ILI9481 driver chips
 * Copyright (c) 2015 Andrej Strancar
 *
 *
 * Original: Copyright (c) 2009 Jean-Christian de Rivaz
 *
 * SPI mods, console support, 320x240 instead of 240x320:
 * Copyright (c) 2012 Jeroen Domburg <jeroen@spritesmods.com>
 *
 * Bits and pieces borrowed from the fsl-lcdpi.c:
 * Copyright (C) 2010-2011 Freescale Semiconductor, Inc. All Rights Reserved.
 * Author: Alison Wang <b18965@freescale.com>
 *         Jason Jin <Jason.jin@freescale.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * The Solomon Systech SSD1289 chip drive TFT screen up to 240x320.
 *
 */

//#define DEBUG

//#define CONFIG_FB_BACKLIGHT

#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/platform_device.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/fb.h>
#include <asm/io.h>
#include <linux/spi/spi.h>
#include <linux/pinctrl/consumer.h>
#include <linux/backlight.h>
#include <linux/dma-mapping.h>

#include <linux/version.h>
#include <linux/ioctl.h>
#include <asm/uaccess.h>

#include <linux/clk.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/semaphore.h>
#include <mach/platform.h>

#define lcdpi_SET_COLUMN_ADDRESS       0x2a
#define lcdpi_SET_PAGE_ADDRESS         0x2b
#define lcdpi_WRITE_MEMORY_START       0x2c

#define REG_PWM_ENABLE (BCM2708_PERI_BASE + 0x20C000)		/* register address for enabling the pwm */
#define REG_PWM_DUTY (BCM2708_PERI_BASE + 0x20C014)			/* register address for setting duty in ns */
#define REG_PWM_PERIOD (BCM2708_PERI_BASE + 0x20C010)		/* register address for setting period in ns */
#define PWMCLK_CNTL (BCM2708_PERI_BASE + 0x1010A0)			/* register address for enabling/disabling the clock */
#define VAL_PWMCLK_CNTL_OFF 0x5A000000 | (1 << 5)	//value to write in the register to disable clock
#define VAL_PWMCLK_CNTL_ON 0x5A000000 | 0x11 		//value to write in register to enable clock
#define PWMCLK_DIV (BCM2708_PERI_BASE + 0x1010A4)
#define VAL_PWM_ENABLE 0x00000001					//value to write in register to enable the pwm
#define VAL_PWM_DISABLE 0xfffffffe					//value to write in register to disable the pwm
#define VAL_PWMCLK_DIV 0x5A000000 | (19 << 12)		//Value to set the clock to ~ 1Mhz
#define FUNC_SLCT_HEAT_PWM (BCM2708_PERI_BASE + 0x200004)		/* register address for setting i/o function */
#define MASK_FUNC_SLCT_HEAT_PWM 0x03800000			//Mask for bits in FUNC_SLCT_HEAT
#define VAL_FUNC_SLCT_HEAT_PWM 0x02000000			//Value to set GPIO17 as output and GPIO18 as PWM0
#define MASK_CTL_PWM 0x000000FF            			//
#define VAL_CTL_PWM 0x00000081            			//Value to enable i/o function as pwm

static volatile unsigned long *ptrPWMCLK_CNTL;
static volatile unsigned long *ptrPWMCLK_DIV;
static volatile unsigned long *ptrREG_PWM_ENABLE;
static volatile unsigned long *ptrREG_PWM_DUTY;
static volatile unsigned long *ptrREG_PWM_PERIOD;
static volatile unsigned long *ptrFUNC_SLCT_HEAT_PWM;

struct lcdpi_page {
	unsigned short x;
	unsigned short y;
	unsigned short *buffer;
	unsigned short len;
	int must_update;
};

struct lcdpi {
	struct device *dev;
	spinlock_t spi_lock;
	struct spi_device *spidev;
	struct fb_info *info;
	unsigned int pages_count;
	struct lcdpi_page *pages;
	unsigned long pseudo_palette[17];
	int backlight;
	int brightness;
	int update_brightness;
};

/* module parameters */
static int rotate = 0;
module_param(rotate, int, 0444);
MODULE_PARM_DESC(rotate, "Screen rotation: 0/90/180/270");

static int compress = 1;
module_param(compress, int, 0444);
MODULE_PARM_DESC(compress, "SPI compresion 1/0");

static int pwm = 1;
module_param(pwm, int, 0444);
MODULE_PARM_DESC(pwm, "PWM backlight control on GPIO18 1/0");

static int product_code = 0x00;
module_param(product_code, int, 0444);
MODULE_PARM_DESC(product_code, "Force product code");

static long sclk = 32000000;
module_param(sclk, long, 0444);
MODULE_PARM_DESC(sclk, "SPI clock frequency");

static int invbl = 0;
module_param(invbl, int, 0444);
MODULE_PARM_DESC(invbl, "Invert bachlight: 0/1");

static long frames=0;
module_param(frames, long, 0444);
MODULE_PARM_DESC(frames, "frame counter");

/* horizontal and vertical resolution */
static u16 HDP=480;
static u16 VDP=272;

/* LCD width and height, depend on rotation! */
static u16 width;
static u16 height;

/* ioctl for reading tactile switches */
#define QUERY_GET_KEYS _IOR('K', 1, unsigned char *)

/* ioctl for reading tactile product code */
#define QUERY_GET_PRODUCT_CODE _IOR('P', 1, unsigned char *)

static int lcdpi_spi_write(struct lcdpi *item, unsigned short value,
				unsigned int isdata);

static int pi_pwm_init(void){
	if(!pwm)
		return 1;

	ptrREG_PWM_ENABLE = ioremap_nocache(REG_PWM_ENABLE,4);
	if(ptrREG_PWM_ENABLE == NULL){
		printk(KERN_INFO "[pwmpi] ioremap REG_PWM_ENABLE failed\n");
	}
	ptrREG_PWM_DUTY = ioremap_nocache(REG_PWM_DUTY,4);
	if(ptrREG_PWM_DUTY == NULL){
		printk(KERN_INFO "[pwmpi] ioremap REG_PWM_DUTY failed\n");
	}


    ptrREG_PWM_PERIOD = (unsigned long *)ioremap_nocache(REG_PWM_PERIOD,4);
	if(ptrREG_PWM_PERIOD == NULL){
		printk(KERN_INFO "[pwmpi] ioremap REG_PWM_PERIOD\n");
	}


    ptrPWMCLK_CNTL = (unsigned long *)ioremap_nocache(PWMCLK_CNTL,4);
	if(ptrPWMCLK_CNTL == NULL){
		printk(KERN_INFO "[pwmpi] ioremap PWMCLK_CNTL failed\n");
	}


    ptrPWMCLK_DIV = (unsigned long *)ioremap_nocache(PWMCLK_DIV,4);
	if(ptrPWMCLK_DIV == NULL){
		printk(KERN_INFO "[pwmpi] ioremap PWMCLK_DIV failed\n");
	}


	ptrFUNC_SLCT_HEAT_PWM = ioremap_nocache(FUNC_SLCT_HEAT_PWM,4);
	if(ptrFUNC_SLCT_HEAT_PWM == NULL){
		printk(KERN_INFO "[pwmpi] ioremap FUNC_SLCT_HEAT_PWM failed\n");
	}


	printk(KERN_INFO "[pwmpi] IOREMAP done");
	mdelay(1);
	iowrite32((ioread32(ptrFUNC_SLCT_HEAT_PWM) & ~MASK_FUNC_SLCT_HEAT_PWM) | VAL_FUNC_SLCT_HEAT_PWM, ptrFUNC_SLCT_HEAT_PWM);	//Set function to i/o
	mdelay(1);
	iowrite32(VAL_PWMCLK_CNTL_OFF, ptrPWMCLK_CNTL);	//First disable clock
	mdelay(1);
	iowrite32(VAL_PWMCLK_DIV, ptrPWMCLK_DIV);		//clock set to 1Mhz.
	mdelay(1);
	iowrite32(VAL_PWMCLK_CNTL_ON, ptrPWMCLK_CNTL);	//Restart the clock
	mdelay(1);
	iowrite32(ioread32(ptrREG_PWM_ENABLE) & ~MASK_CTL_PWM, ptrREG_PWM_ENABLE);
    mdelay(1);
    iowrite32(ioread32(ptrREG_PWM_ENABLE) | VAL_CTL_PWM, ptrREG_PWM_ENABLE);    //Enable pwm
	printk(KERN_INFO "[pwmpi] clock set");
	//printk("alt:%x",ioread32(ptrFUNC_SLCT_HEAT_PWM));

	return 0;
}

static int pi_pwm_config(int duty_ns, int period_ns){
	u32 period32=period_ns;
	u32 duty32=duty_ns;

	if(!pwm)
			return 1;

	mdelay(1);
	iowrite32(duty32, ptrREG_PWM_DUTY);
	mdelay(1);
	iowrite32(period32, ptrREG_PWM_PERIOD);
//	printk(KERN_INFO "[pwmpi] duty:%d period:%d\n",ioread32(ptrREG_PWM_DUTY),ioread32(ptrREG_PWM_PERIOD));
	return 0;
}


static int pi_pwm_enable(void){
	if(!pwm)
			return 1;

	mdelay(1);
	iowrite32(ioread32(ptrREG_PWM_ENABLE) | VAL_PWM_ENABLE, ptrREG_PWM_ENABLE);
	return 0;
}


static void pi_pwm_disable(void){
	if(!pwm)
			return;

	mdelay(1);
	iowrite32(ioread32(ptrREG_PWM_ENABLE) & VAL_PWM_DISABLE, ptrREG_PWM_ENABLE);
}

static int lcdpi_backlight_update_status(struct backlight_device *bd) {
	struct lcdpi *item = bl_get_data(bd);

//	dev_info(item->dev,
//			"bl update status: power=%x fb_blank=%x brightness=%x\n",
//			bd->props.power, bd->props.fb_blank, bd->props.brightness);

//	if ((bd->props.power == FB_BLANK_UNBLANK)
//			&& (bd->props.fb_blank == FB_BLANK_UNBLANK))
//		item->backlight = 1;
//	else
//		brightness = 0;

	if(bd->props.fb_blank == 0)
		item->brightness = bd->props.brightness;
	else
		item->brightness = 0;
	pi_pwm_config(item->brightness * 4, 255*4);
	item->update_brightness = 1;
	item->pages[0].must_update=1;
	schedule_delayed_work(&item->info->deferred_work, HZ / 20);

	return 0;
}

static int lcdpi_backlight_get_brightness(struct backlight_device *bd)
{
        return bd->props.brightness;
}

static void lcdpi_unregister_backlight(struct lcdpi *item)
{
        const struct backlight_ops *bl_ops;

         if (item->info->bl_dev) {
                item->info->bl_dev->props.power = FB_BLANK_POWERDOWN;
                backlight_update_status(item->info->bl_dev);
                bl_ops = item->info->bl_dev->ops;
                backlight_device_unregister(item->info->bl_dev);
                item->info->bl_dev = NULL;
                kfree(bl_ops);
        }
}

static void lcdpi_register_backlight(struct lcdpi *item) {
	struct backlight_device *bd;
	struct backlight_properties bl_props = {0, 255,	};
	struct backlight_ops *bl_ops;

	bl_ops = kzalloc(sizeof(struct backlight_ops), GFP_KERNEL);
	if (!bl_ops) {
		dev_err(item->info->device,
				"%s: could not allocate memeory for backlight operations.\n",
				__func__);
		return;
	}

	bl_ops->get_brightness = lcdpi_backlight_get_brightness;
	bl_ops->update_status = lcdpi_backlight_update_status;
	bl_props.type = BACKLIGHT_RAW;
	/* Assume backlight is off, get polarity from current state of pin */
	bl_props.power = FB_BLANK_POWERDOWN;
//        if (!gpio_get_value(info->gpio.led[0]))
//                bl_props.state |= BL_CORE_DRIVER1;

	bd = backlight_device_register(dev_driver_string(item->info->device),
			item->info->device, item, bl_ops, &bl_props);
	if (IS_ERR(bd)) {
		dev_err(item->info->device, "cannot register backlight device (%ld)\n",
				PTR_ERR(bd));
		goto failed;
	}
	item->info->bl_dev = bd;

//        if (!item->fbtftops.unregister_backlight)
//                item->fbtftops.unregister_backlight = lcdpi_unregister_backlight;

	pi_pwm_config(255 * 4, 255*4);
	item->info->bl_dev->props.brightness = 255;
	return;

	failed: kfree(bl_ops);
}


static struct spi_device *lcdpi_spi_device;


#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int lcdpi_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg)
#else
static int lcdpi_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg)
#endif
{
	unsigned char keys;

	switch (cmd)
		{
			case QUERY_GET_KEYS:
            lcdpi_spi_device->max_speed_hz=16000000;
			lcdpi_spi_device->master->setup(lcdpi_spi_device);
	        spi_read(lcdpi_spi_device, &keys, sizeof(unsigned char));
	        lcdpi_spi_device->max_speed_hz=sclk;
	        lcdpi_spi_device->master->setup(lcdpi_spi_device);
			keys = (keys >> 3) & 0x1f;
			if (copy_to_user((unsigned char *)arg, &keys, sizeof(unsigned char)))
			{
				return -EACCES;
			}
			break;

			case QUERY_GET_PRODUCT_CODE:
			if (copy_to_user((unsigned char *)arg, &product_code, sizeof(unsigned char)))
			{
				return -EACCES;
			}
			break;

			default:
			return -EINVAL;
		}

	return 0;
}


//#define BLOCKLEN (HDP * VDP * 2 + 10)
#define BLOCKLEN 65536
static u8 *lcdpi_spiblock;

#define MAX_DMA_PIXELS 32767
static struct {
	u8 *buf;
	dma_addr_t dma;
	size_t len;
} lcdpi_spiblock_dma;

static int write_spi_dma(struct lcdpi *item, size_t len)
{
        struct spi_transfer t = {
                .tx_buf = lcdpi_spiblock_dma.buf,
                .len = len,
        };
        struct spi_message m;

        item->spidev->max_speed_hz=sclk;
        item->spidev->mode=0;
        item->spidev->master->setup(item->spidev);

        spi_message_init(&m);
        t.tx_dma = lcdpi_spiblock_dma.dma;
        m.is_dma_mapped = 1;
        spi_message_add_tail(&t, &m);
        return spi_sync(item->spidev, &m);
}

/*
This routine will write a single 16-bit value, either as data or as a command
(depends on isdata). The LCD will clock this in because the SPIs /CS goes high.
*/

/*
 * bits in control byte
 */
#define PI32_LONG (1<<7)
#define PI32_BLOCK (1<<6)
#define PI32_RESET (1<<5)
#define PI32_RS (1<<4)
#define PI32_BL (1<<3)
#define PI32_RD (1<<2)

static int lcdpi_spi_write(struct lcdpi *item, unsigned short value,
				unsigned int isdata)
{

    item->spidev->max_speed_hz=16000000;
    item->spidev->mode=0;
    item->spidev->master->setup(item->spidev);

	//item->backlight = 1;
	item->backlight = item->info->bl_dev->props.brightness;

	/*
	 * control byte
	 */
	lcdpi_spiblock[0]=((isdata?PI32_RS:0)|(item->backlight?PI32_BL:0))|PI32_RESET|PI32_RD;
	/*
	 * 16-bit data / command
	 */
	lcdpi_spiblock[1]=(value>>8)&0xff;
	lcdpi_spiblock[2]=(value)&0xff;

	spi_write(item->spidev, lcdpi_spiblock, 3);
	return 0;
}

static inline void lcdpi_reg_set(struct lcdpi *item, unsigned char reg,
				   unsigned short value)
{
	lcdpi_spi_write(item, reg&0xff, 0);
	lcdpi_spi_write(item, value, 1);
}

static inline void lcdpi_spi_write_array(struct lcdpi *item,
					u8 *value, u8 len)
{
	u8 i;

	lcdpi_spi_write(item, value[0]&0xff, 0);
	for(i=1; i < len; i++)
		lcdpi_spi_write(item, value[i], 1);
}

static void lcdpi_update_all(struct lcdpi *item)
{
	unsigned short i;
	struct fb_deferred_io *fbdefio = item->info->fbdefio;
	for (i = 0; i < item->pages_count; i++) {
		item->pages[i].must_update=1;
	}
	schedule_delayed_work(&item->info->deferred_work, fbdefio->delay);
}

extern unsigned int compress_rgb(unsigned char *dest, unsigned int ptr, unsigned short *buffer, int blen, unsigned char compress_speedup);

static void lcdpi_update(struct fb_info *info, struct list_head *pagelist)
{
	struct lcdpi *item = (struct lcdpi *)info->par;
	struct page *page;
	int i;
	int ptr;
	unsigned short *buffer;
	int lasti, blen;
	unsigned short compress_speedup = 7;

	if(sclk > 48000000) {
        compress_speedup = 4;
    }

	frames++;

	if(item->update_brightness == 1) {
		item->update_brightness = 0;
	}

	//We can be called because of pagefaults (mmap'ed framebuffer, pages
	//returned in *pagelist) or because of kernel activity
	//(pages[i]/must_update!=0). Add the former to the list of the latter.
	list_for_each_entry(page, pagelist, lru) {
		item->pages[page->index].must_update=1;
	}

	//Copy changed pages.

	lasti = -2;
	ptr = 0;

	for (i=0; i<item->pages_count; i++) {
		//ToDo: Small race here between checking and setting must_update,
		//maybe lock?
		if (item->pages[i].must_update) {
			item->pages[i].must_update=0;

			buffer = item->pages[i].buffer;
			blen = item->pages[i].len;

			if(ptr >= MAX_DMA_PIXELS - item->pages[i].len) {
				pr_debug("Transfering block of max=%d bytes\n", ptr*4);
				lasti = -2;
			}

			if(i != lasti + 1) {
				if(ptr) {
					write_spi_dma(item, ptr*2+2);
					ptr = 0;
					pr_debug("Transfering block of %d bytes\n", ptr*4);
				}

				lcdpi_spi_write(item, lcdpi_SET_COLUMN_ADDRESS, 0);
				lcdpi_spi_write(item, 0, 1);
				lcdpi_spi_write(item, 0, 1);
				lcdpi_spi_write(item, width>>8, 1);
				lcdpi_spi_write(item, width&0x00ff, 1);
				lcdpi_spi_write(item, lcdpi_SET_PAGE_ADDRESS, 0);
				lcdpi_spi_write(item, item->pages[i].y >> 8, 1);
				lcdpi_spi_write(item, item->pages[i].y &0x0ff, 1);
				lcdpi_spi_write(item, height>>8, 1);
				lcdpi_spi_write(item, height&0x00ff, 1);
				lcdpi_spi_write(item, lcdpi_WRITE_MEMORY_START, 0);

				/*
				 * control byte for long data transfer. Any number of 16-bit data can follow.
				 */
				lcdpi_spiblock_dma.buf[0]=PI32_RS|(item->backlight?PI32_BL:0)|PI32_RESET|PI32_RD|PI32_LONG|PI32_BLOCK;
			}

			if((ptr == 0) && (i != 0)) {
				buffer -= item->pages[i].x;
				blen += item->pages[i].x;
			}

            ptr = compress_rgb(lcdpi_spiblock_dma.buf, ptr, buffer, blen, compress_speedup);
			lasti = i;
			pr_debug("Added page %d, ptr=%d\n", i, ptr);
		}
	}


	if (ptr) {
		pr_debug("Transfering block of %d bytes [%2x, %2x, %2x, ...]\n",
				ptr * 2 + 1, lcdpi_spiblock[0], lcdpi_spiblock[1], lcdpi_spiblock[2]);
		//item->spidev->max_speed_hz=64000000;
		//item->spidev->master->setup(item->spidev);

		/*
		 * long transfer. Control byte + ptr of 16-bit data
		 */
		lcdpi_spiblock_dma.buf[(ptr * 2) + 1] = 0x00;
		write_spi_dma(item, ptr * 2 + 2);
		ptr = 0;
	}

}

static void lcdpi_update_noc(struct fb_info *info, struct list_head *pagelist)
{
	struct lcdpi *item = (struct lcdpi *)info->par;
	struct page *page;
	int i;
	int x, lasti, ptr, blen;
	unsigned short *buffer, value;
	//struct timeval tv1, tv2;

	frames++;

	if(item->update_brightness == 1) {
		lcdpi_spi_write(item, 0x00BE, 0); //set PWM for B/L
		mdelay(1);
		lcdpi_spi_write(item, 0x0006, 1);
		mdelay(1);
		lcdpi_spi_write(item, item->brightness, 1);
		mdelay(1);
		lcdpi_spi_write(item, 0x0001, 1);
		mdelay(1);
		lcdpi_spi_write(item, item->brightness, 1);
		mdelay(1);
		lcdpi_spi_write(item, 0x0000, 1);
		mdelay(1);
		lcdpi_spi_write(item, 0x0000, 1);
		mdelay(1);
		item->update_brightness = 0;
	}

	//We can be called because of pagefaults (mmap'ed framebuffer, pages
	//returned in *pagelist) or because of kernel activity
	//(pages[i]/must_update!=0). Add the former to the list of the latter.
	list_for_each_entry(page, pagelist, lru) {
		item->pages[page->index].must_update=1;
	}

	//Copy changed pages.

	lasti = -2;
	ptr = 0;

	for (i=0; i<item->pages_count; i++) {
		//ToDo: Small race here between checking and setting must_update,
		//maybe lock?
		if (item->pages[i].must_update) {
			item->pages[i].must_update=0;

			buffer = item->pages[i].buffer;
			blen = item->pages[i].len;

			if(ptr >= MAX_DMA_PIXELS - item->pages[i].len) {
				pr_debug("Transfering block of max=%d bytes\n", ptr*4);
				lasti = -2;
			}

			if(i != lasti + 1) {
				if(ptr) {
					write_spi_dma(item, ptr*2+2);
					ptr = 0;
					pr_debug("Transfering block of %d bytes\n", ptr*4);
				}

				lcdpi_spi_write(item, lcdpi_SET_COLUMN_ADDRESS, 0);
				lcdpi_spi_write(item, 0, 1);
				lcdpi_spi_write(item, 0, 1);
				lcdpi_spi_write(item, width>>8, 1);
				lcdpi_spi_write(item, width&0x00ff, 1);
				lcdpi_spi_write(item, lcdpi_SET_PAGE_ADDRESS, 0);
				lcdpi_spi_write(item, item->pages[i].y >> 8, 1);
				lcdpi_spi_write(item, item->pages[i].y &0x0ff, 1);
				lcdpi_spi_write(item, height>>8, 1);
				lcdpi_spi_write(item, height&0x00ff, 1);
				lcdpi_spi_write(item, lcdpi_WRITE_MEMORY_START, 0);

				/*
				 * control byte for long data transfer. Any number of 16-bit data can follow.
				 */
				lcdpi_spiblock_dma.buf[0]=PI32_RS|(item->backlight?PI32_BL:0)|PI32_RESET|PI32_RD|PI32_LONG;
			}

			if((ptr == 0) && (i != 0)) {
				buffer -= item->pages[i].x;
				blen += item->pages[i].x;
			}

			for (x=0; x<blen; x++) {
				value=buffer[x];
				//value = (buffer[x] & 0x07e0) + (buffer[x] >> 11) + (buffer[x] << 11);
				lcdpi_spiblock_dma.buf[(ptr*2)+1]=(value>>8)&0xff;
				lcdpi_spiblock_dma.buf[(ptr*2)+2]=(value)&0xff;
				ptr++;
			}

			lasti = i;
			pr_debug("Added page %d, ptr=%d\n", i, ptr);
		}
	}

	if(ptr) {
		pr_debug("Transfering block of %d bytes [%2x, %2x, %2x, ...]\n", ptr*2+1, lcdpi_spiblock[0],
				lcdpi_spiblock[1], lcdpi_spiblock[2]);
//		item->spidev->max_speed_hz=sclk;
//		item->spidev->master->setup(item->spidev);

		/*
		 * long transfer. Control byte + ptr of 16-bit data
		 */
		write_spi_dma(item, ptr*2+2);
		ptr = 0;
	}

}

static void __init init_ili9481(struct lcdpi *item) {
    lcdpi_spiblock[1]=0;
    lcdpi_spiblock[2]=0;
    lcdpi_spiblock[0]=PI32_RESET|PI32_RD;
    spi_write(item->spidev, lcdpi_spiblock, 3);
    mdelay(50);
    lcdpi_spiblock[0]=PI32_RD;
    spi_write(item->spidev, lcdpi_spiblock, 3);
    mdelay(50);
    lcdpi_spiblock[0]=PI32_RESET|PI32_RD;
    spi_write(item->spidev, lcdpi_spiblock, 3);
    mdelay(120);

    lcdpi_spi_write(item, 0x11, 0);
    mdelay(20);
    lcdpi_spi_write(item, 0xD0, 0);
    lcdpi_spi_write(item, 0x07, 1);
    lcdpi_spi_write(item, 0x42, 1);
    lcdpi_spi_write(item, 0x18, 1);

    lcdpi_spi_write(item, 0xD1, 0);
    lcdpi_spi_write(item, 0x00, 1);
    lcdpi_spi_write(item, 0x07, 1);
    lcdpi_spi_write(item, 0x10, 1);

    lcdpi_spi_write(item, 0xD2, 0);
    lcdpi_spi_write(item, 0x01, 1);
    lcdpi_spi_write(item, 0x02, 1);

    lcdpi_spi_write(item, 0xC0, 0);
    lcdpi_spi_write(item, 0x10, 1);
    lcdpi_spi_write(item, 0x3B, 1);
    lcdpi_spi_write(item, 0x00, 1);
    lcdpi_spi_write(item, 0x02, 1);
    lcdpi_spi_write(item, 0x11, 1);

    lcdpi_spi_write(item, 0xC5, 0);
    lcdpi_spi_write(item, 0x03, 1);

    lcdpi_spi_write(item, 0xC8, 0);
    lcdpi_spi_write(item, 0x00, 1);
    lcdpi_spi_write(item, 0x32, 1);
    lcdpi_spi_write(item, 0x36, 1);
    lcdpi_spi_write(item, 0x45, 1);
    lcdpi_spi_write(item, 0x06, 1);
    lcdpi_spi_write(item, 0x16, 1);
    lcdpi_spi_write(item, 0x37, 1);
    lcdpi_spi_write(item, 0x75, 1);
    lcdpi_spi_write(item, 0x77, 1);
    lcdpi_spi_write(item, 0x54, 1);
    lcdpi_spi_write(item, 0x0C, 1);
    lcdpi_spi_write(item, 0x00, 1);

    lcdpi_spi_write(item, 0x36, 0);

    switch(rotate) {
        case 180:
            lcdpi_spi_write(item, 0x28, 1);
            break;
        case 270:
            lcdpi_spi_write(item, 0x0A, 1);
            break;
        case 90:
            lcdpi_spi_write(item, 0x09, 1);
            break;
        default:
            lcdpi_spi_write(item, 0x2B, 1);
            break;
    };



    lcdpi_spi_write(item, 0x3A, 0);
    lcdpi_spi_write(item, 0x55, 1);

    lcdpi_spi_write(item, 0x2A, 0);
    lcdpi_spi_write(item, 0x00, 1);
    lcdpi_spi_write(item, 0x00, 1);
    lcdpi_spi_write(item, 0x01, 1);
    lcdpi_spi_write(item, 0x3F, 1);

    lcdpi_spi_write(item, 0x2B, 0);
    lcdpi_spi_write(item, 0x00, 1);
    lcdpi_spi_write(item, 0x00, 1);
    lcdpi_spi_write(item, 0x01, 1);
    lcdpi_spi_write(item, 0xE0, 1);
    mdelay(120);
    lcdpi_spi_write(item, 0x29, 0);
}


static void __init init_ili9486(struct lcdpi *item) {
	lcdpi_spiblock[1]=0;
	lcdpi_spiblock[2]=0;
	lcdpi_spiblock[0]=PI32_RESET|PI32_RD;
	spi_write(item->spidev, lcdpi_spiblock, 3);
	mdelay(50);
	lcdpi_spiblock[0]=PI32_RD;
	spi_write(item->spidev, lcdpi_spiblock, 3);
	mdelay(50);
	lcdpi_spiblock[0]=PI32_RESET|PI32_RD;
	spi_write(item->spidev, lcdpi_spiblock, 3);
	mdelay(120);

	lcdpi_spi_write(item, 0x11, 0);
	mdelay(50);
	lcdpi_spi_write(item, 0xF2, 0);
	lcdpi_spi_write(item, 0x1C, 1);
	lcdpi_spi_write(item, 0xA3, 1);
	lcdpi_spi_write(item, 0x32, 1);
	lcdpi_spi_write(item, 0x02, 1);
	lcdpi_spi_write(item, 0xB2, 1);
	lcdpi_spi_write(item, 0x12, 1);
	lcdpi_spi_write(item, 0xFF, 1);
	lcdpi_spi_write(item, 0x12, 1);
	lcdpi_spi_write(item, 0x00, 1);

	lcdpi_spi_write(item, 0xF1, 0);
	lcdpi_spi_write(item, 0x36, 1);
	lcdpi_spi_write(item, 0xA4, 1);

	lcdpi_spi_write(item, 0xF8, 0);
	lcdpi_spi_write(item, 0x21, 1);
	lcdpi_spi_write(item, 0x04, 1);

	lcdpi_spi_write(item, 0xF9, 0);
	lcdpi_spi_write(item, 0x00, 1);
	lcdpi_spi_write(item, 0x08, 1);

	lcdpi_spi_write(item, 0xC0, 0);
	lcdpi_spi_write(item, 0x0D, 1);
	lcdpi_spi_write(item, 0x0D, 1);

	lcdpi_spi_write(item, 0xC1, 0);
	lcdpi_spi_write(item, 0x43, 1);
	lcdpi_spi_write(item, 0x00, 1);

	lcdpi_spi_write(item, 0xC2, 0);
	lcdpi_spi_write(item, 0x00, 1);

	lcdpi_spi_write(item, 0xC5, 0);
	lcdpi_spi_write(item, 0x00, 1);
	lcdpi_spi_write(item, 0x48, 1);

	lcdpi_spi_write(item, 0xB6, 0);
	lcdpi_spi_write(item, 0x00, 1);
	lcdpi_spi_write(item, 0x22, 1);
	lcdpi_spi_write(item, 0x3B, 1);

	lcdpi_spi_write(item, 0xE0, 0);
	lcdpi_spi_write(item, 0x0F, 1);
	lcdpi_spi_write(item, 0x24, 1);
	lcdpi_spi_write(item, 0x1C, 1);
	lcdpi_spi_write(item, 0x0A, 1);
	lcdpi_spi_write(item, 0x0F, 1);
	lcdpi_spi_write(item, 0x08, 1);
	lcdpi_spi_write(item, 0x43, 1);
	lcdpi_spi_write(item, 0x88, 1);
	lcdpi_spi_write(item, 0x32, 1);
	lcdpi_spi_write(item, 0x0F, 1);
	lcdpi_spi_write(item, 0x10, 1);
	lcdpi_spi_write(item, 0x06, 1);
	lcdpi_spi_write(item, 0x0F, 1);
	lcdpi_spi_write(item, 0x07, 1);
	lcdpi_spi_write(item, 0x00, 1);

	lcdpi_spi_write(item, 0xE1, 0);
	lcdpi_spi_write(item, 0x0F, 1);
	lcdpi_spi_write(item, 0x38, 1);
	lcdpi_spi_write(item, 0x30, 1);
	lcdpi_spi_write(item, 0x09, 1);
	lcdpi_spi_write(item, 0x0F, 1);
	lcdpi_spi_write(item, 0x0F, 1);
	lcdpi_spi_write(item, 0x4E, 1);
	lcdpi_spi_write(item, 0x77, 1);
	lcdpi_spi_write(item, 0x3C, 1);
	lcdpi_spi_write(item, 0x07, 1);
	lcdpi_spi_write(item, 0x10, 1);
	lcdpi_spi_write(item, 0x05, 1);
	lcdpi_spi_write(item, 0x23, 1);
	lcdpi_spi_write(item, 0x1B, 1);
	lcdpi_spi_write(item, 0x00, 1);

	lcdpi_spi_write(item, 0x20, 0);
	lcdpi_spi_write(item, 0x00, 1);

	lcdpi_spi_write(item, 0x36, 0);
	switch(rotate) {
		case 180:
			lcdpi_spi_write(item, 0xa8, 1);
			break;
		case 270:
			lcdpi_spi_write(item, 0xc8, 1);
			break;
		case 90:
			lcdpi_spi_write(item, 0x08, 1);
			break;
		default:
			lcdpi_spi_write(item, 0x68, 1);
			break;
	};

	lcdpi_spi_write(item, 0x3A, 0);
	lcdpi_spi_write(item, 0x55, 1);

	lcdpi_spi_write(item, 0x2A, 0);
	lcdpi_spi_write(item, 0x00, 1);
	lcdpi_spi_write(item, 0x00, 1);
	lcdpi_spi_write(item, 0x01, 1);
	lcdpi_spi_write(item, 0xDF, 1);

	lcdpi_spi_write(item, 0x2B, 0);
	lcdpi_spi_write(item, 0x00, 1);
	lcdpi_spi_write(item, 0x00, 1);
	lcdpi_spi_write(item, 0x01, 1);
	lcdpi_spi_write(item, 0xDF, 1);

	mdelay(50);

	lcdpi_spi_write(item, 0x29, 0);
	lcdpi_spi_write(item, 0x2C, 0);
}

//This routine will allocate the buffer for the complete framebuffer. This
//is one continuous chunk of 16-bit pixel values; userspace programs
//will write here.
static int __init lcdpi_video_alloc(struct lcdpi *item)
{
	unsigned int frame_size;

	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	frame_size = item->info->fix.line_length * item->info->var.yres;
	dev_dbg(item->dev, "%s: item=0x%p frame_size=%u\n",
		__func__, (void *)item, frame_size);

	item->pages_count = frame_size / PAGE_SIZE;
	if ((item->pages_count * PAGE_SIZE) < frame_size) {
		item->pages_count++;
	}
	dev_dbg(item->dev, "%s: item=0x%p pages_count=%u\n",
		__func__, (void *)item, item->pages_count);

	item->info->fix.smem_len = item->pages_count * PAGE_SIZE;
	item->info->fix.smem_start =
	    (unsigned long)vmalloc(item->info->fix.smem_len);
	if (!item->info->fix.smem_start) {
		dev_err(item->dev, "%s: unable to vmalloc\n", __func__);
		return -ENOMEM;
	}
	memset((void *)item->info->fix.smem_start, 0, item->info->fix.smem_len);

	return 0;
}

static void lcdpi_video_free(struct lcdpi *item)
{
	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	// kfree((void *)item->info->fix.smem_start);
	vfree((void *)item->info->fix.smem_start);
}

//This routine will allocate a lcdpi_page struct for each vm page in the
//main framebuffer memory. Each struct will contain a pointer to the page
//start, an x- and y-offset, and the length of the pagebuffer which is in the framebuffer.
static int __init lcdpi_pages_alloc(struct lcdpi *item)
{
	unsigned short pixels_per_page;
	unsigned short yoffset_per_page;
	unsigned short xoffset_per_page;
	unsigned short index;
	unsigned short x = 0;
	unsigned short y = 0;
	unsigned short *buffer;
	unsigned int len;

	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	item->pages = kmalloc(item->pages_count * sizeof(struct lcdpi_page),
			      GFP_KERNEL);
	if (!item->pages) {
		dev_err(item->dev, "%s: unable to kmalloc for lcdpi_page\n",
			__func__);
		return -ENOMEM;
	}

	pixels_per_page = PAGE_SIZE / (item->info->var.bits_per_pixel / 8);
	yoffset_per_page = pixels_per_page / item->info->var.xres;
	xoffset_per_page = pixels_per_page -
	    (yoffset_per_page * item->info->var.xres);
	dev_dbg(item->dev, "%s: item=0x%p pixels_per_page=%hu "
		"yoffset_per_page=%hu xoffset_per_page=%hu\n",
		__func__, (void *)item, pixels_per_page,
		yoffset_per_page, xoffset_per_page);

	buffer = (unsigned short *)item->info->fix.smem_start;
	for (index = 0; index < item->pages_count; index++) {
		len = (item->info->var.xres * item->info->var.yres) -
		    (index * pixels_per_page);
		if (len > pixels_per_page) {
			len = pixels_per_page;
		}
		dev_dbg(item->dev,
			"%s: page[%d]: x=%3hu y=%3hu buffer=0x%p len=%3hu\n",
			__func__, index, x, y, buffer, len);
		item->pages[index].x = x;
		item->pages[index].y = y;
		item->pages[index].buffer = buffer;
		item->pages[index].len = len;

		x += xoffset_per_page;
		if (x >= item->info->var.xres) {
			y++;
			x -= item->info->var.xres;
		}
		y += yoffset_per_page;
		buffer += pixels_per_page;
	}

	return 0;
}

static void lcdpi_pages_free(struct lcdpi *item)
{
	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	kfree(item->pages);
	// vfree(item->pages);
}

static inline __u32 CNVT_TOHW(__u32 val, __u32 width)
{
	return ((val<<width) + 0x7FFF - val)>>16;
}

//This routine is needed because the console driver won't work without it.
static int lcdpi_setcolreg(unsigned regno,
			       unsigned red, unsigned green, unsigned blue,
			       unsigned transp, struct fb_info *info)
{
	int ret = 1;

	/*
	 * If greyscale is true, then we convert the RGB value
	 * to greyscale no matter what visual we are using.
	 */
	if (info->var.grayscale)
		red = green = blue = (19595 * red + 38470 * green +
				      7471 * blue) >> 16;
	switch (info->fix.visual) {
	case FB_VISUAL_TRUECOLOR:
		if (regno < 16) {
			u32 *pal = info->pseudo_palette;
			u32 value;

			red = CNVT_TOHW(red, info->var.red.length);
			green = CNVT_TOHW(green, info->var.green.length);
			blue = CNVT_TOHW(blue, info->var.blue.length);
			transp = CNVT_TOHW(transp, info->var.transp.length);

			value = (red << info->var.red.offset) |
				(green << info->var.green.offset) |
				(blue << info->var.blue.offset) |
				(transp << info->var.transp.offset);

			pal[regno] = value;
			ret = 0;
		}
		break;
	case FB_VISUAL_STATIC_PSEUDOCOLOR:
	case FB_VISUAL_PSEUDOCOLOR:
		break;
	}
	return ret;
}

static int lcdpi_blank(int blank_mode, struct fb_info *info)
{
	struct lcdpi *item = (struct lcdpi *)info->par;
	if (blank_mode == FB_BLANK_UNBLANK)
		item->backlight=1;
	else
		item->backlight=0;
	//Item->backlight won't take effect until the LCD is written to. Force that
	//by dirty'ing a page.
	item->pages[0].must_update=1;
	schedule_delayed_work(&info->deferred_work, 0);
	return 0;
}

static void lcdpi_touch(struct fb_info *info, int x, int y, int w, int h)
{
	struct fb_deferred_io *fbdefio = info->fbdefio;
	struct lcdpi *item = (struct lcdpi *)info->par;
	int i, ystart, yend;
	if (fbdefio) {
		//Touch the pages the y-range hits, so the deferred io will update them.
		for (i=0; i<item->pages_count; i++) {
			ystart=item->pages[i].y;
			yend=item->pages[i].y+(item->pages[i].len/info->fix.line_length)+1;
			if (!((y+h)<ystart || y>yend)) {
				item->pages[i].must_update=1;
			}
		}
		//Schedule the deferred IO to kick in after a delay.
		schedule_delayed_work(&info->deferred_work, fbdefio->delay);
	}
}

static void lcdpi_fillrect(struct fb_info *p, const struct fb_fillrect *rect)
{
	sys_fillrect(p, rect);
	lcdpi_touch(p, rect->dx, rect->dy, rect->width, rect->height);
}

static void lcdpi_imageblit(struct fb_info *p, const struct fb_image *image)
{
	sys_imageblit(p, image);
	lcdpi_touch(p, image->dx, image->dy, image->width, image->height);
}

static void lcdpi_copyarea(struct fb_info *p, const struct fb_copyarea *area)
{
	sys_copyarea(p, area);
	lcdpi_touch(p, area->dx, area->dy, area->width, area->height);
}

static ssize_t lcdpi_write(struct fb_info *p, const char __user *buf,
				size_t count, loff_t *ppos)
{
	ssize_t res;
	res = fb_sys_write(p, buf, count, ppos);
	lcdpi_touch(p, 0, 0, p->var.xres, p->var.yres);
	return res;
}

static struct fb_ops lcdpi_fbops = {
	.owner        = THIS_MODULE,
	.fb_read      = fb_sys_read,
	.fb_write     = lcdpi_write,
	.fb_fillrect  = lcdpi_fillrect,
	.fb_copyarea  = lcdpi_copyarea,
	.fb_imageblit = lcdpi_imageblit,
	.fb_setcolreg	= lcdpi_setcolreg,
	.fb_blank	= lcdpi_blank,
	.fb_ioctl = lcdpi_ioctl,
};

static struct fb_fix_screeninfo lcdpi_fix __initdata = {
	.id          = "lcdpi",
	.type        = FB_TYPE_PACKED_PIXELS,
	.visual      = FB_VISUAL_TRUECOLOR,
	.accel       = FB_ACCEL_NONE,
//	.line_length =  HDP * 2,
};

static struct fb_var_screeninfo lcdpi_var __initdata = {
//		.xres		= HDP,
//		.yres		= VDP,
//		.xres_virtual	= HDP,
//		.yres_virtual	= VDP,
		.width		= 74,
		.height		= 49,
		.bits_per_pixel	= 16,
		.red		= {11, 5, 0},
		.green		= {5, 6, 0},
		.blue		= {0, 5, 0},
		.activate	= FB_ACTIVATE_NOW,
		.vmode		= FB_VMODE_NONINTERLACED,
};

static struct fb_deferred_io lcdpi_defio = {
        .delay          = HZ / 60,
        .deferred_io    = &lcdpi_update,
};

static u32 lcdpi_of_value(struct device_node *node, const char *propname)
{
    u32 val = -1;

    of_property_read_u32(node, propname, &val);
    return val;
}

static int lcdpi_parse_dt(struct device *dev)
{
    struct device_node *node = dev->of_node;
    u32 dummy;

    if(!node)
        return -1;

    of_property_read_u8(node, "pi40,product-code", (u8*)&product_code);
    of_property_read_u8(node, "pi40,invert-backlight", (u8*)&invbl);

    dummy = lcdpi_of_value(node, "rotate");
    if(dummy != -1)
        rotate = dummy;

    dummy = lcdpi_of_value(node, "spi-max-frequency");
    if(dummy != -1)
        sclk = dummy;

    return 0;
}

static int __init lcdpi_probe(struct spi_device *dev)
{
	int ret = 0;
	struct lcdpi *item;
	struct fb_info *info;

	dev_dbg(&dev->dev, "%s\n", __func__);

    lcdpi_parse_dt(&dev->dev);

	item = kzalloc(sizeof(struct lcdpi), GFP_KERNEL);
	if (!item) {
		dev_err(&dev->dev,
			"%s: unable to kzalloc for lcdpi\n", __func__);
		ret = -ENOMEM;
		goto out;
	}
	item->dev = &dev->dev;
	dev_set_drvdata(&dev->dev, item);
	item->backlight=1;

	lcdpi_spi_device=item->spidev=dev;
	spin_lock_init(&item->spi_lock);
	item->dev=&dev->dev;
	spi_set_drvdata(dev, item);
	dev_info(&dev->dev, "spi registered, item=0x%p\n", (void *)item);

	lcdpi_spiblock = (u8*)kmalloc(10, GFP_KERNEL);

    item->spidev->max_speed_hz=16000000;
    item->spidev->mode=0;
    item->spidev->master->setup(item->spidev);

	//reset display and get the pruduct code
	lcdpi_spiblock[1]=0;
	lcdpi_spiblock[2]=0;
	lcdpi_spiblock[0]=PI32_RESET|PI32_RD;
	spi_write(item->spidev, lcdpi_spiblock, 3);
	mdelay(50);
	lcdpi_spiblock[0]=PI32_RD;
	spi_write(item->spidev, lcdpi_spiblock, 3);
	mdelay(50);

    if(!product_code) {
        spi_read(lcdpi_spi_device, &product_code, sizeof(unsigned char));
	}

    HDP = 480;
    VDP = 320;

	printk("product_code=0x%x\n", product_code);

    switch(product_code) {
        case 0x38:
            rotate = (rotate + 180) % 360;
            break;
        default:
            break;
    }

    switch(rotate) {
		case 90:
		case 270: lcdpi_var.yres = HDP;
		lcdpi_var.xres = VDP;
		lcdpi_var.yres_virtual = HDP;
		lcdpi_var.xres_virtual = VDP;
		lcdpi_var.width = 49;
		lcdpi_var.height = 74;
		lcdpi_fix.line_length =  VDP * 2;
		width = VDP - 1;
		height = HDP - 1;
		break;

		default: lcdpi_var.xres = HDP;
		lcdpi_var.yres = VDP;
		lcdpi_var.xres_virtual = HDP;
		lcdpi_var.yres_virtual = VDP;
		lcdpi_var.width = 74;
		lcdpi_var.height = 49;
		lcdpi_fix.line_length =  HDP * 2;
		width = HDP - 1;
		height = VDP - 1;
		break;
	}

	info = framebuffer_alloc(sizeof(struct lcdpi), &dev->dev);
	if (!info) {
		ret = -ENOMEM;
		dev_err(&dev->dev,
			"%s: unable to framebuffer_alloc\n", __func__);
		goto out_item;
	}

	info->pseudo_palette = &item->pseudo_palette;
	item->info = info;
	info->par = item;
	info->dev = &dev->dev;
	info->fbops = &lcdpi_fbops;
	info->flags = FBINFO_FLAG_DEFAULT|FBINFO_VIRTFB;
	info->fix = lcdpi_fix;
	info->var = lcdpi_var;

	ret = lcdpi_video_alloc(item);
	if (ret) {
		dev_err(&dev->dev,
			"%s: unable to lcdpi_video_alloc\n", __func__);
		goto out_info;
	}
	info->screen_base = (char __iomem *)item->info->fix.smem_start;

	ret = lcdpi_pages_alloc(item);
	if (ret < 0) {
		dev_err(&dev->dev,
			"%s: unable to lcdpi_pages_init\n", __func__);
		goto out_video;
	}

	pi_pwm_init();
	pi_pwm_enable();
	lcdpi_register_backlight(item);

	switch(compress) {
		case 1:
		lcdpi_defio.deferred_io = &lcdpi_update;
		break;

		case 0:
		lcdpi_defio.deferred_io = &lcdpi_update_noc;
		break;
	}

//	info->fbdefio = &lcdpi_defio;
//	fb_deferred_io_init(info);

	item->dev->coherent_dma_mask = ~0;
//	lcdpi_spiblock = (u8*)kmalloc(BLOCKLEN * 4, GFP_KERNEL);
	lcdpi_spiblock_dma.buf = dma_alloc_coherent(item->dev, BLOCKLEN * 4, &lcdpi_spiblock_dma.dma, GFP_DMA);
	lcdpi_spiblock_dma.len = BLOCKLEN * 4;

	if(!lcdpi_spiblock){
		dev_err(&dev->dev,
					"%s: unable to allocate spi buffer\n", __func__);
		goto out_pages;
	}

	switch(product_code) {
    case 0x38:
        init_ili9481(item);
		break;
	default:
        init_ili9486(item);
		break;
	}

	info->fbdefio = &lcdpi_defio;
	fb_deferred_io_init(info);
	lcdpi_update_all(item);

	ret = register_framebuffer(info);
	if (ret < 0) {
		dev_err(&dev->dev,
			"%s: unable to register_frambuffer\n", __func__);
		goto out_pages;
	}

	return ret;

out_pages:
	lcdpi_unregister_backlight(item);
	pi_pwm_disable();
	lcdpi_pages_free(item);
out_video:
	lcdpi_video_free(item);
out_info:
	framebuffer_release(info);
out_item:
	kfree(item);
out:
	return ret;
}

static int __exit lcdpi_remove(struct spi_device *dev)
{
   struct lcdpi *item = dev_get_drvdata(&dev->dev);
   struct fb_info *info;

   dev_dbg(&dev->dev, "%s\n", __func__);

   dev_set_drvdata(&dev->dev, NULL);
   if (item) {
	   	   lcdpi_unregister_backlight(item);
	   	   pi_pwm_disable();
           info = item->info;
           if (info) {
        	   fb_deferred_io_cleanup(info);
               unregister_framebuffer(info);
               framebuffer_release(info);
           }
           lcdpi_pages_free(item);
           lcdpi_video_free(item);

           if(lcdpi_spiblock_dma.buf)
        	   dma_free_coherent(item->dev, BLOCKLEN * 4, lcdpi_spiblock_dma.buf, lcdpi_spiblock_dma.dma);

           kfree(item);
   }

   if(lcdpi_spiblock)
	   kfree(lcdpi_spiblock);

   return 0;

}

	/*
#ifdef CONFIG_PM
static int lcdpi_suspend(struct spi_device *spi, pm_message_t state)
{
	struct fb_info *info = dev_get_drvdata(&spi->dev);
	struct lcdpi *item = (struct lcdpi *)info->par;
	// enter into sleep mode
	lcdpi_reg_set(item, lcdpi_REG_SLEEP_MODE, 0x0001);
	return 0;
}

static int lcdpi_resume(struct spi_device *spi)
{
	struct fb_info *info = dev_get_drvdata(&spi->dev);
	struct lcdpi *item = (struct lcdpi *)info->par;
	// leave sleep mode
	lcdpi_reg_set(item, lcdpi_REG_SLEEP_MODE, 0x0000);
	return 0;
}
#else
*/
#define lcdpi_suspend NULL
#define lcdpi_resume NULL
//#endif

static const struct of_device_id lcdpi_dt_ids[] = {
    { .compatible = "hwlevel,ips40" },
    {},
};

MODULE_DEVICE_TABLE(of, lcdpi_dt_ids);

static struct spi_driver spi_lcdpi_driver = {
	.driver = {
        .name	= "lcdpi-ips40",
		.bus	= &spi_bus_type,
		.owner	= THIS_MODULE,
        .of_match_table = of_match_ptr(lcdpi_dt_ids),
	},
	.probe = lcdpi_probe,
	.remove = lcdpi_remove,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,1,0))
    .suspend = lcdpi_suspend,
    .resume = lcdpi_resume,
#endif
};

static int __init lcdpi_init(void)
{
	int ret = 0;

	ret = spi_register_driver(&spi_lcdpi_driver);
	if (ret) {
		pr_err("%s: unable to platform_driver_register\n", __func__);
	}

	return ret;
}
static void __exit lcdpi_exit(void)
{
       pr_debug("%s\n", __func__);

       spi_unregister_driver(&spi_lcdpi_driver);
}

module_init(lcdpi_init);
module_exit(lcdpi_exit);

MODULE_DESCRIPTION("ILI9486/ILI9481 LCD Driver");
MODULE_AUTHOR("Andrej Strancar <info@hwlevel.com>");
MODULE_LICENSE("GPL");
