/*
 * Framebuffer driver for 320x240 Hwlevel display modules
 * based on SSD1289 driver chip
 * Copyright (c) 2015 Andrej Strancar
 *
 * Original: Copyright (c) 2009 Jean-Christian de Rivaz
 *
 * SPI mods, console support, 320x240 instead of 240x320:
 * Copyright (c) 2012 Jeroen Domburg <jeroen@spritesmods.com>
 *
 * Bits and pieces borrowed from the fsl-ssd1289.c:
 * Copyright (C) 2010-2011 Freescale Semiconductor, Inc. All Rights Reserved.
 * Author: Alison Wang <b18965@freescale.com>
 *         Jason Jin <Jason.jin@freescale.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * The Solomon Systech SSD1289 chip drive TFT screen up to 240x320.
 *
 */

#define QUERY_GET_VARIABLES _IOR('K', 1, unsigned char *)
//#define QUERY_RESET _IOR('R', 2, unsigned char *)

//#define DEBUG

#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/fb.h>
#include <asm/io.h>
#include <linux/spi/spi.h>
#include <linux/pinctrl/consumer.h>
#include <linux/timer.h>
#include <linux/dma-mapping.h>

#include <linux/version.h>
#include <linux/ioctl.h>
#include <asm/uaccess.h>

#include <linux/clk.h>
#include <linux/time.h>
#include <linux/delay.h>

#define SSD1289_REG_OSCILLATION      0x00
#define SSD1289_REG_DRIVER_OUT_CTRL  0x01
#define SSD1289_REG_LCD_DRIVE_AC     0x02
#define SSD1289_REG_POWER_CTRL_1     0x03
#define SSD1289_REG_DISPLAY_CTRL     0x07
#define SSD1289_REG_FRAME_CYCLE      0x0b
#define SSD1289_REG_POWER_CTRL_2     0x0c
#define SSD1289_REG_POWER_CTRL_3     0x0d
#define SSD1289_REG_POWER_CTRL_4     0x0e
#define SSD1289_REG_GATE_SCAN_START  0x0f
#define SSD1289_REG_SLEEP_MODE       0x10
#define SSD1289_REG_ENTRY_MODE       0x11
#define SSD1289_REG_POWER_CTRL_5     0x1e
#define SSD1289_REG_GDDRAM_DATA      0x22
#define SSD1289_REG_WR_DATA_MASK_1   0x23
#define SSD1289_REG_WR_DATA_MASK_2   0x24
#define SSD1289_REG_FRAME_FREQUENCY  0x25
#define SSD1289_REG_GAMMA_CTRL_1     0x30
#define SSD1289_REG_GAMME_CTRL_2     0x31
#define SSD1289_REG_GAMMA_CTRL_3     0x32
#define SSD1289_REG_GAMMA_CTRL_4     0x33
#define SSD1289_REG_GAMMA_CTRL_5     0x34
#define SSD1289_REG_GAMMA_CTRL_6     0x35
#define SSD1289_REG_GAMMA_CTRL_7     0x36
#define SSD1289_REG_GAMMA_CTRL_8     0x37
#define SSD1289_REG_GAMMA_CTRL_9     0x3a
#define SSD1289_REG_GAMMA_CTRL_10    0x3b
#define SSD1289_REG_V_SCROLL_CTRL_1  0x41
#define SSD1289_REG_V_SCROLL_CTRL_2  0x42
#define SSD1289_REG_H_RAM_ADR_POS    0x44
#define SSD1289_REG_V_RAM_ADR_START  0x45
#define SSD1289_REG_V_RAM_ADR_END    0x46
#define SSD1289_REG_FIRST_WIN_START  0x48
#define SSD1289_REG_FIRST_WIN_END    0x49
#define SSD1289_REG_SECND_WIN_START  0x4a
#define SSD1289_REG_SECND_WIN_END    0x4b
#define SSD1289_REG_GDDRAM_X_ADDR    0x4e
#define SSD1289_REG_GDDRAM_Y_ADDR    0x4f

struct ssd1289_page {
    unsigned short x;
    unsigned short y;
    unsigned short *buffer;
    unsigned short len;
    int must_update;
};

struct ssd1289 {
    struct device *dev;
    spinlock_t spi_lock;
    struct spi_device *spidev;
//	struct {
//		void *buf;
//		dma_addr_t dma;
//		size_t len;
//	} txbuf;
    struct fb_info *info;
    unsigned int pages_count;
    struct ssd1289_page *pages;
    unsigned long pseudo_palette[17];
    int backlight;
};

/* module parameters */
static int rotate = 180;
module_param(rotate, int, 0444);
MODULE_PARM_DESC(rotate, "Screen rotation: 0/90/180/270");

static long sclk = 48000000;
module_param(sclk, long, 0444);
MODULE_PARM_DESC(sclk, "SPI clock frequency");



static int ssd1289_spi_write(struct ssd1289 *item, unsigned short value,
        unsigned int isdata);

static int ssd1289_backlight_update_status(struct backlight_device *bd)
{
    struct ssd1289 *item = bl_get_data(bd);

    if(bd->props.fb_blank == 0)
        item->backlight = bd->props.brightness;
    else
        item->backlight = 0;

    //ssd1289_spi_write(item, 0x0000, 0); //NOP
    item->pages[0].must_update=1;
    schedule_delayed_work(&item->info->deferred_work, 0);

    return 0;
}

static int ssd1289_backlight_get_brightness(struct backlight_device *bd)
{
    return bd->props.brightness;
}

static void ssd1289_unregister_backlight(struct ssd1289 *item)
{
    const struct backlight_ops *bl_ops;

    if (item->info->bl_dev) {
        item->info->bl_dev->props.power = FB_BLANK_POWERDOWN;
        backlight_update_status(item->info->bl_dev);
        bl_ops = item->info->bl_dev->ops;
        backlight_device_unregister(item->info->bl_dev);
        item->info->bl_dev = NULL;
        kfree(bl_ops);
    }
}

static void ssd1289_register_backlight(struct ssd1289 *item)
{
    struct backlight_device *bd;
    struct backlight_properties bl_props = { 0, 1, };
    struct backlight_ops *bl_ops;

    bl_ops = kzalloc(sizeof(struct backlight_ops), GFP_KERNEL);
    if (!bl_ops) {
        dev_err(item->info->device,
                "%s: could not allocate memeory for backlight operations.\n",
                __func__);
        return;
    }

    bl_ops->get_brightness = ssd1289_backlight_get_brightness;
    bl_ops->update_status = ssd1289_backlight_update_status;
    bl_props.type = BACKLIGHT_RAW;
    /* Assume backlight is off, get polarity from current state of pin */
    bl_props.power = FB_BLANK_POWERDOWN;
    //        if (!gpio_get_value(info->gpio.led[0]))
    //                bl_props.state |= BL_CORE_DRIVER1;

    bd = backlight_device_register(dev_driver_string(item->info->device),
            item->info->device, item, bl_ops, &bl_props);
    if (IS_ERR(bd)) {
        dev_err(item->info->device,
                "cannot register backlight device (%ld)\n",
                PTR_ERR(bd));
        goto failed;
    }
    item->info->bl_dev = bd;

    //        if (!item->fbtftops.unregister_backlight)
    //                item->fbtftops.unregister_backlight = ssd1289_unregister_backlight;

    item->info->bl_dev->props.brightness = 1;
    return;

    failed:
    kfree(bl_ops);
}

static struct spi_device *ssd1289_spi_device;
static struct ssd1289 *item;

static void __init ssd1289_setup(struct ssd1289 *item);

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int ssd1289_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg)
#else
static int ssd1289_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg)
#endif
{
    unsigned char keys;

    switch (cmd)
    {
    case QUERY_GET_VARIABLES:
        spi_read(ssd1289_spi_device, &keys, sizeof(unsigned char));
        keys = (keys >> 3) & 0x1f;
        if (copy_to_user((unsigned char *)arg, &keys, sizeof(unsigned char)))
        {
            return -EACCES;
        }
        break;

        //case QUERY_RESET:
            //    ssd1289_setup(item);
            //    break;

        default:
            return -EINVAL;
    }

    return 0;
}

//#define BLOCKLEN (320 * 240 * 2 + 1)
#define BLOCKLEN 65536
static u8 *ssd1289_spiblock;

#define MAX_DMA_PIXELS 32767
static struct {
    u8 *buf;
    dma_addr_t dma;
    size_t len;
} lcdpi_spiblock_dma;

static int write_spi_dma(struct ssd1289 *item, size_t len)
{
        struct spi_transfer t = {
                .tx_buf = lcdpi_spiblock_dma.buf,
                .len = len,
        };
        struct spi_message m;

        spi_message_init(&m);
        t.tx_dma = lcdpi_spiblock_dma.dma;
        m.is_dma_mapped = 1;
        spi_message_add_tail(&t, &m);
        return spi_sync(item->spidev, &m);
}

/*
This routine will write a single 16-bit value, either as data or as a command
(depends on isdata). The LCD will clock this in because the SPIs /CS goes high.
 */

/*
 * bits in control byte
 */
#define PI32_LONG (1<<7)
#define PI32_RESET (1<<5)
#define PI32_RS (1<<4)
#define PI32_BL (1<<3)
#define PI32_RD (1<<2)

static int ssd1289_spi_write(struct ssd1289 *item, unsigned short value,
        unsigned int isdata)
{
//	item->spidev->max_speed_hz=sclk;
//	item->spidev->mode=0;
//	item->spidev->master->setup(item->spidev);

    //item->backlight = 1;
    //item->backlight = item->info->bl_dev->props.brightness;

    /*
     * control byte
     */
    ssd1289_spiblock[0]=((isdata?PI32_RS:0)|(item->backlight?PI32_BL:0))|PI32_RESET|PI32_RD;

    /*
     * 16-bit data / command
     */
    ssd1289_spiblock[1]=(value>>8)&0xff;
    ssd1289_spiblock[2]=(value)&0xff;

    spi_write(item->spidev, ssd1289_spiblock, 3);
    return 0;
}

static inline void ssd1289_reg_set(struct ssd1289 *item, unsigned char reg,
        unsigned short value)
{
    ssd1289_spi_write(item, reg&0xff, 0);
    ssd1289_spi_write(item, value, 1);
}

static void ssd1289_update_all(struct ssd1289 *item)
{
    unsigned short i;
    struct fb_deferred_io *fbdefio = item->info->fbdefio;
    for (i = 0; i < item->pages_count; i++) {
        item->pages[i].must_update=1;
    }
    schedule_delayed_work(&item->info->deferred_work, fbdefio->delay);
}

static void ssd1289_update(struct fb_info *info, struct list_head *pagelist)
{
    struct ssd1289 *item = (struct ssd1289 *)info->par;
    struct page *page;
    int i;
    int x, lasti, ptr, updated = 0;
    unsigned short *buffer, value;
    //struct timeval tv1, tv2;

    //We can be called because of pagefaults (mmap'ed framebuffer, pages
    //returned in *pagelist) or because of kernel activity
    //(pages[i]/must_update!=0). Add the former to the list of the latter.
    list_for_each_entry(page, pagelist, lru) {
        item->pages[page->index].must_update=1;
    }

    //Copy changed pages.

    lasti = -2;
    ptr = 0;

    for (i=0; i<item->pages_count; i++) {
        //ToDo: Small race here between checking and setting must_update,
        //maybe lock?
        if (item->pages[i].must_update) {
            item->pages[i].must_update=0;
            updated = 1;

            buffer = item->pages[i].buffer;

            if(ptr >= MAX_DMA_PIXELS - item->pages[i].len) {
                pr_debug("Transfering block of max=%d bytes\n", ptr*4);
                lasti = -2;
            }

            if(i != lasti + 1) {
                if(ptr) {
                    write_spi_dma(item, ptr*2+2);
                    ptr = 0;
                    pr_debug("Transfering block of %d bytes\n", ptr*4);
                }

                switch(rotate) {
                case 0:
                    ssd1289_reg_set(item, SSD1289_REG_GDDRAM_X_ADDR, (item->info->var.yres - 1)-item->pages[i].y);
                    ssd1289_reg_set(item, SSD1289_REG_GDDRAM_Y_ADDR, item->pages[i].x);
                    break;
                case 180:
                    ssd1289_reg_set(item, SSD1289_REG_GDDRAM_X_ADDR, item->pages[i].y);
                    ssd1289_reg_set(item, SSD1289_REG_GDDRAM_Y_ADDR, (item->info->var.xres - 1) - item->pages[i].x);
                    break;
                }
                ssd1289_spi_write(item, SSD1289_REG_GDDRAM_DATA, 0);
                pr_debug("Starting new block\n");

                /*
                 * control byte for long data transfer. Any number of 16-bit data can follow.
                 */
                lcdpi_spiblock_dma.buf[0]=PI32_RS|(item->backlight?PI32_BL:0)|PI32_RESET|PI32_RD|PI32_LONG;
            }

            for (x=0; x<item->pages[i].len; x++) {
                value=buffer[x];
                lcdpi_spiblock_dma.buf[(ptr*2)+1]=(value>>8)&0xff;
                lcdpi_spiblock_dma.buf[(ptr*2)+2]=(value)&0xff;
                ptr++;
            }

            lasti = i;
            pr_debug("Added page %d, ptr=%d\n", i, ptr);
        }
    }

    if(ptr) {
        pr_debug("Transfering block of %d bytes [%2x, %2x, %2x, ...]\n", ptr*2+1, ssd1289_spiblock[0],
                ssd1289_spiblock[1], ssd1289_spiblock[2]);
        //item->spidev->max_speed_hz=64000000;
        //item->spidev->master->setup(item->spidev);

        //do_gettimeofday(&tv1);

        /*
         * long transfer. Control byte + ptr of 16-bit data
         */
        write_spi_dma(item, ptr * 2 + 2);

        //ssd1289_spi_write(item, SSD1289_REG_GDDRAM_DATA, 0);
        //spi_write(item->spidev, &ssd1289_spiblock[1], ptr*2);
        //do_gettimeofday(&tv2);
        //pr_debug("Time ellapsed: %d us\n", tv2.tv_usec - tv1.tv_usec);
        ptr = 0;
    }
}

static void __init ssd1289_setup(struct ssd1289 *item)
{
    dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);
    // OSCEN=1

    item->spidev->max_speed_hz=sclk/4;
    item->spidev->mode=0;
    item->spidev->master->setup(item->spidev);

    ssd1289_spiblock[1]=0;
    ssd1289_spiblock[2]=0;
    ssd1289_spiblock[0]=PI32_RS|PI32_BL|PI32_RD|PI32_RESET;
    spi_write(item->spidev, ssd1289_spiblock, 3);
    mdelay(50);
    ssd1289_spiblock[0]=PI32_RS|PI32_RD;
    spi_write(item->spidev, ssd1289_spiblock, 3);
    mdelay(50);
    ssd1289_spiblock[0]=PI32_RS|PI32_BL|PI32_RD|PI32_RESET;
    spi_write(item->spidev, ssd1289_spiblock, 3);
    mdelay(250);

    ssd1289_reg_set(item, 0x0028,0x0006);
    ssd1289_reg_set(item, 0x0000,0x0001);
    ssd1289_reg_set(item, 0x0003,0xaea4);    /* power control 1---line frequency and VHG,VGL voltage */
    ssd1289_reg_set(item, 0x000c,0x0004);    /* power control 2---VCIX2 output voltage */
    ssd1289_reg_set(item, 0x000d,0x000c);    /* power control 3---Vlcd63 voltage */
    ssd1289_reg_set(item, 0x000e,0x2800);    /* power control 4---VCOMA voltage VCOML=VCOMH*0.9475-VCOMA */
    ssd1289_reg_set(item, 0x001e,0x00b5);    /* POWER CONTROL 5---VCOMH voltage */
    ssd1289_reg_set(item, 0x0001,0x3b3f);
    ssd1289_reg_set(item, 0x0002,0x0600);
    ssd1289_reg_set(item, 0x0010,0x0000);
    switch(rotate) {
    case 180:
        ssd1289_reg_set(item, 0x0011,0x6098);
        break;
    case 0:
        ssd1289_reg_set(item, 0x0011,0x60a8);
        break;
    }
    ssd1289_reg_set(item, 0x0005,0x0000);
    ssd1289_reg_set(item, 0x0006,0x0000);
    ssd1289_reg_set(item, 0x0016,0xef1c);
    ssd1289_reg_set(item, 0x0007,0x0033);    /* Display control 1 */
    /* when GON=1 and DTE=0,all gate outputs become VGL */
    /* when GON=1 and DTE=0,all gate outputs become VGH */
    /* non-selected gate wires become VGL */
    ssd1289_reg_set(item, 0x000b,0x0000);
    ssd1289_reg_set(item, 0x000f,0x0000);
    ssd1289_reg_set(item, 0x0041,0x0000);
    ssd1289_reg_set(item, 0x0042,0x0000);
    ssd1289_reg_set(item, 0x0048,0x0000);
    ssd1289_reg_set(item, 0x0049,0x013f);
    ssd1289_reg_set(item, 0x004a,0x0000);
    ssd1289_reg_set(item, 0x004b,0x0000);
    ssd1289_reg_set(item, 0x0044,0xef00);	/* Horizontal RAM start and end address */
    ssd1289_reg_set(item, 0x0045,0x0000);	/* Vretical RAM start address */
    ssd1289_reg_set(item, 0x0046,0x013f);	/* Vretical RAM end address */
    ssd1289_reg_set(item, 0x004e,0x0000);	/* set GDDRAM x address counter */
    ssd1289_reg_set(item, 0x004f,0x0000);    /* set GDDRAM y address counter */
    /* y control */
    ssd1289_reg_set(item, 0x0030,0x0707);
    ssd1289_reg_set(item, 0x0031,0x0202);
    ssd1289_reg_set(item, 0x0032,0x0204);
    ssd1289_reg_set(item, 0x0033,0x0502);
    ssd1289_reg_set(item, 0x0034,0x0507);
    ssd1289_reg_set(item, 0x0035,0x0204);
    ssd1289_reg_set(item, 0x0036,0x0204);
    ssd1289_reg_set(item, 0x0037,0x0502);
    ssd1289_reg_set(item, 0x003a,0x0302);
    ssd1289_reg_set(item, 0x003b,0x0302);
    ssd1289_reg_set(item, 0x0023,0x0000);
    ssd1289_reg_set(item, 0x0024,0x0000);
    ssd1289_reg_set(item, 0x0025,0x8000);
    ssd1289_reg_set(item, 0x0026,0x7000);
    ssd1289_reg_set(item, 0x0020,0xb0eb);
    ssd1289_reg_set(item, 0x0027,0x007c);
    ssd1289_spi_write(item, 0x0022, 0);

    item->spidev->max_speed_hz=sclk;
    item->spidev->mode=0;
    item->spidev->master->setup(item->spidev);
}

//This routine will allocate the buffer for the complete framebuffer. This
//is one continuous chunk of 16-bit pixel values; userspace programs
//will write here.
static int __init ssd1289_video_alloc(struct ssd1289 *item)
{
    unsigned int frame_size;

    dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

    frame_size = item->info->fix.line_length * item->info->var.yres;
    dev_dbg(item->dev, "%s: item=0x%p frame_size=%u\n",
            __func__, (void *)item, frame_size);

    item->pages_count = frame_size / PAGE_SIZE;
    if ((item->pages_count * PAGE_SIZE) < frame_size) {
        item->pages_count++;
    }
    dev_dbg(item->dev, "%s: item=0x%p pages_count=%u\n",
            __func__, (void *)item, item->pages_count);

    item->info->fix.smem_len = item->pages_count * PAGE_SIZE;
    item->info->fix.smem_start =
            (unsigned long)vmalloc(item->info->fix.smem_len);
    if (!item->info->fix.smem_start) {
        dev_err(item->dev, "%s: unable to vmalloc\n", __func__);
        return -ENOMEM;
    }
    memset((void *)item->info->fix.smem_start, 0, item->info->fix.smem_len);

    return 0;
}

static void ssd1289_video_free(struct ssd1289 *item)
{
    dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

    // kfree((void *)item->info->fix.smem_start);
    vfree((void *)item->info->fix.smem_start);
}

//This routine will allocate a ssd1289_page struct for each vm page in the
//main framebuffer memory. Each struct will contain a pointer to the page
//start, an x- and y-offset, and the length of the pagebuffer which is in the framebuffer.
static int __init ssd1289_pages_alloc(struct ssd1289 *item)
{
    unsigned short pixels_per_page;
    unsigned short yoffset_per_page;
    unsigned short xoffset_per_page;
    unsigned short index;
    unsigned short x = 0;
    unsigned short y = 0;
    unsigned short *buffer;
    unsigned int len;

    dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

    item->pages = kmalloc(item->pages_count * sizeof(struct ssd1289_page),
            GFP_KERNEL);
    if (!item->pages) {
        dev_err(item->dev, "%s: unable to kmalloc for ssd1289_page\n",
                __func__);
        return -ENOMEM;
    }

    pixels_per_page = PAGE_SIZE / (item->info->var.bits_per_pixel / 8);
    yoffset_per_page = pixels_per_page / item->info->var.xres;
    xoffset_per_page = pixels_per_page -
            (yoffset_per_page * item->info->var.xres);
    dev_dbg(item->dev, "%s: item=0x%p pixels_per_page=%hu "
            "yoffset_per_page=%hu xoffset_per_page=%hu\n",
            __func__, (void *)item, pixels_per_page,
            yoffset_per_page, xoffset_per_page);

    buffer = (unsigned short *)item->info->fix.smem_start;
    for (index = 0; index < item->pages_count; index++) {
        len = (item->info->var.xres * item->info->var.yres) -
                (index * pixels_per_page);
        if (len > pixels_per_page) {
            len = pixels_per_page;
        }
        dev_dbg(item->dev,
                "%s: page[%d]: x=%3hu y=%3hu buffer=0x%p len=%3hu\n",
                __func__, index, x, y, buffer, len);
        item->pages[index].x = x;
        item->pages[index].y = y;
        item->pages[index].buffer = buffer;
        item->pages[index].len = len;

        x += xoffset_per_page;
        if (x >= item->info->var.xres) {
            y++;
            x -= item->info->var.xres;
        }
        y += yoffset_per_page;
        buffer += pixels_per_page;
    }

    return 0;
}

static void ssd1289_pages_free(struct ssd1289 *item)
{
    dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

    kfree(item->pages);
    // vfree(item->pages);
}

static inline __u32 CNVT_TOHW(__u32 val, __u32 width)
{
    return ((val<<width) + 0x7FFF - val)>>16;
}

//This routine is needed because the console driver won't work without it.
static int ssd1289_setcolreg(unsigned regno,
        unsigned red, unsigned green, unsigned blue,
        unsigned transp, struct fb_info *info)
{
    int ret = 1;

    /*
     * If greyscale is true, then we convert the RGB value
     * to greyscale no matter what visual we are using.
     */
    if (info->var.grayscale)
        red = green = blue = (19595 * red + 38470 * green +
                7471 * blue) >> 16;
    switch (info->fix.visual) {
    case FB_VISUAL_TRUECOLOR:
        if (regno < 16) {
            u32 *pal = info->pseudo_palette;
            u32 value;

            red = CNVT_TOHW(red, info->var.red.length);
            green = CNVT_TOHW(green, info->var.green.length);
            blue = CNVT_TOHW(blue, info->var.blue.length);
            transp = CNVT_TOHW(transp, info->var.transp.length);

            value = (red << info->var.red.offset) |
                    (green << info->var.green.offset) |
                    (blue << info->var.blue.offset) |
                    (transp << info->var.transp.offset);

            pal[regno] = value;
            ret = 0;
        }
        break;
    case FB_VISUAL_STATIC_PSEUDOCOLOR:
    case FB_VISUAL_PSEUDOCOLOR:
        break;
    }
    return ret;
}

static int ssd1289_blank(int blank_mode, struct fb_info *info)
{
    struct ssd1289 *item = (struct ssd1289 *)info->par;
    if (blank_mode == FB_BLANK_UNBLANK)
        item->backlight=1;
    else
        item->backlight=0;
    //Item->backlight won't take effect until the LCD is written to. Force that
    //by dirty'ing a page.
    item->pages[0].must_update=1;
    schedule_delayed_work(&info->deferred_work, 0);
    return 0;
}

static void ssd1289_touch(struct fb_info *info, int x, int y, int w, int h)
{
    struct fb_deferred_io *fbdefio = info->fbdefio;
    struct ssd1289 *item = (struct ssd1289 *)info->par;
    int i, ystart, yend;
    if (fbdefio) {
        //Touch the pages the y-range hits, so the deferred io will update them.
        for (i=0; i<item->pages_count; i++) {
            ystart=item->pages[i].y;
            yend=item->pages[i].y+(item->pages[i].len/info->fix.line_length)+1;
            if (!((y+h)<ystart || y>yend)) {
                item->pages[i].must_update=1;
            }
        }
        //Schedule the deferred IO to kick in after a delay.
        schedule_delayed_work(&info->deferred_work, fbdefio->delay);
    }
}

static void ssd1289_fillrect(struct fb_info *p, const struct fb_fillrect *rect)
{
    sys_fillrect(p, rect);
    ssd1289_touch(p, rect->dx, rect->dy, rect->width, rect->height);
}

static void ssd1289_imageblit(struct fb_info *p, const struct fb_image *image)
{
    sys_imageblit(p, image);
    ssd1289_touch(p, image->dx, image->dy, image->width, image->height);
}

static void ssd1289_copyarea(struct fb_info *p, const struct fb_copyarea *area)
{
    sys_copyarea(p, area);
    ssd1289_touch(p, area->dx, area->dy, area->width, area->height);
}

static ssize_t ssd1289_write(struct fb_info *p, const char __user *buf,
        size_t count, loff_t *ppos)
{
    ssize_t res;
    res = fb_sys_write(p, buf, count, ppos);
    ssd1289_touch(p, 0, 0, p->var.xres, p->var.yres);
    return res;
}

static struct fb_ops ssd1289_fbops = {
        .owner        = THIS_MODULE,
        .fb_read      = fb_sys_read,
        .fb_write     = ssd1289_write,
        .fb_fillrect  = ssd1289_fillrect,
        .fb_copyarea  = ssd1289_copyarea,
        .fb_imageblit = ssd1289_imageblit,
        .fb_setcolreg	= ssd1289_setcolreg,
        .fb_blank	= ssd1289_blank,
        .fb_ioctl = ssd1289_ioctl,
};

static struct fb_fix_screeninfo ssd1289_fix __initdata = {
        .id          = "SSD1289",
        .type        = FB_TYPE_PACKED_PIXELS,
        .visual      = FB_VISUAL_TRUECOLOR,
        .accel       = FB_ACCEL_NONE,
        .line_length = 320 * 2,
};

static struct fb_var_screeninfo ssd1289_var __initdata = {
        .xres		= 320,
        .yres		= 240,
        .xres_virtual	= 320,
        .yres_virtual	= 240,
        .width		= 65,
        .height		= 44,
        .bits_per_pixel	= 16,
        .red		= {11, 5, 0},
        .green		= {5, 6, 0},
        .blue		= {0, 5, 0},
        .activate	= FB_ACTIVATE_NOW,
        .vmode		= FB_VMODE_NONINTERLACED,
};

static struct fb_deferred_io ssd1289_defio = {
        .delay          = HZ / 40,
        .deferred_io    = &ssd1289_update,
};

static u32 lcdpi_of_value(struct device_node *node, const char *propname)
{
    u32 val = -1;

    of_property_read_u32(node, propname, &val);
    return val;
}

static int lcdpi_parse_dt(struct device *dev)
{
    struct device_node *node = dev->of_node;
    u32 dummy;

    if(!node)
        return -1;


    dummy = lcdpi_of_value(node, "rotate");
    if(dummy != -1)
        rotate = dummy;

    dummy = lcdpi_of_value(node, "spi-max-frequency");
    if(dummy != -1)
        sclk = dummy;

    return 0;
}

static int __init ssd1289_probe(struct spi_device *dev)
{
    int ret = 0;
    struct fb_info *info;

    dev_dbg(&dev->dev, "%s\n", __func__);

    lcdpi_parse_dt(&dev->dev);

    item = kzalloc(sizeof(struct ssd1289), GFP_KERNEL);
    if (!item) {
        dev_err(&dev->dev,
                "%s: unable to kzalloc for ssd1289\n", __func__);
        ret = -ENOMEM;
        goto out;
    }
    item->dev = &dev->dev;
    dev_set_drvdata(&dev->dev, item);
    item->backlight=1;

    ssd1289_spi_device=item->spidev=dev;
    spin_lock_init(&item->spi_lock);
    item->dev=&dev->dev;
    spi_set_drvdata(dev, item);
    dev_info(&dev->dev, "spi registered, item=0x%p\n", (void *)item);

    info = framebuffer_alloc(sizeof(struct ssd1289), &dev->dev);
    if (!info) {
        ret = -ENOMEM;
        dev_err(&dev->dev,
                "%s: unable to framebuffer_alloc\n", __func__);
        goto out_item;
    }
    info->pseudo_palette = &item->pseudo_palette;
    item->info = info;
    info->par = item;
    info->dev = &dev->dev;
    info->fbops = &ssd1289_fbops;
    info->flags = FBINFO_FLAG_DEFAULT|FBINFO_VIRTFB;
    info->fix = ssd1289_fix;
    info->var = ssd1289_var;

    ret = ssd1289_video_alloc(item);
    if (ret) {
        dev_err(&dev->dev,
                "%s: unable to ssd1289_video_alloc\n", __func__);
        goto out_info;
    }
    info->screen_base = (char __iomem *)item->info->fix.smem_start;

    ret = ssd1289_pages_alloc(item);
    if (ret < 0) {
        dev_err(&dev->dev,
                "%s: unable to ssd1289_pages_init\n", __func__);
        goto out_video;
    }

    ssd1289_register_backlight(item);

    info->fbdefio = &ssd1289_defio;
    fb_deferred_io_init(info);

    item->dev->coherent_dma_mask = ~0;
    lcdpi_spiblock_dma.buf = dma_alloc_coherent(item->dev, BLOCKLEN * 4, &lcdpi_spiblock_dma.dma, GFP_DMA);
    lcdpi_spiblock_dma.len = BLOCKLEN * 4;

    ssd1289_spiblock = (u8*)kmalloc(10, GFP_KERNEL);
    if(!ssd1289_spiblock){
        dev_err(&dev->dev,
                "%s: unable to allocate spi buffer\n", __func__);
        goto out_pages;
    }

    ret = register_framebuffer(info);
    if (ret < 0) {
        dev_err(&dev->dev,
                "%s: unable to register_frambuffer\n", __func__);
        goto out_pages;
    }

    ssd1289_setup(item);
    ssd1289_update_all(item);
    ssd1289_setup(item);
    ssd1289_update_all(item);

    return ret;

    out_pages:
    ssd1289_unregister_backlight(item);
    ssd1289_pages_free(item);
    out_video:
    ssd1289_video_free(item);
    out_info:
    framebuffer_release(info);
    out_item:
    kfree(item);
    out:
    return ret;
}

static int __exit ssd1289_remove(struct spi_device *dev)
{
    struct ssd1289 *item = dev_get_drvdata(&dev->dev);
    struct fb_info *info;

    dev_dbg(&dev->dev, "%s\n", __func__);

    dev_set_drvdata(&dev->dev, NULL);
    if (item) {
        ssd1289_unregister_backlight(item);
        info = item->info;
        if (info) {
           fb_deferred_io_cleanup(info);
           unregister_framebuffer(info);
           framebuffer_release(info);
        }
        ssd1289_pages_free(item);
        ssd1289_video_free(item);

        if(lcdpi_spiblock_dma.buf)
           dma_free_coherent(item->dev, BLOCKLEN * 4, lcdpi_spiblock_dma.buf, lcdpi_spiblock_dma.dma);

        kfree(item);
    }
    if(ssd1289_spiblock)
        kfree(ssd1289_spiblock);
    return 0;
}

#ifdef CONFIG_PM
static int ssd1289_suspend(struct spi_device *spi, pm_message_t state)
{
	struct fb_info *info = dev_get_drvdata(&spi->dev);
	struct ssd1289 *item = (struct ssd1289 *)info->par;
	/* enter into sleep mode */
	ssd1289_reg_set(item, SSD1289_REG_SLEEP_MODE, 0x0001);
	return 0;
}

static int ssd1289_resume(struct spi_device *spi)
{
	struct fb_info *info = dev_get_drvdata(&spi->dev);
	struct ssd1289 *item = (struct ssd1289 *)info->par;
	/* leave sleep mode */
	ssd1289_reg_set(item, SSD1289_REG_SLEEP_MODE, 0x0000);
	return 0;
}
#else
#define ssd1289_suspend NULL
#define ssd1289_resume NULL
#endif

static const struct of_device_id ssd1289_dt_ids[] = {
    { .compatible = "solomon,ssd1289" },
    {},
};

MODULE_DEVICE_TABLE(of, ssd1289_dt_ids);
static struct spi_driver spi_ssd1289_driver = {
		.driver = {
                .name	= "lcdpi-ssd1289",
                //.bus	= &spi_bus_type,
				.owner	= THIS_MODULE,
                .of_match_table = of_match_ptr(ssd1289_dt_ids),
		},
		.probe = ssd1289_probe,
		.remove = ssd1289_remove,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,1,0))
    .suspend = ssd1289_suspend,
    .resume = ssd1289_resume,
#endif
};

static int __init ssd1289_init(void)
{
	int ret = 0;

	ret = spi_register_driver(&spi_ssd1289_driver);
	if (ret) {
		pr_err("%s: unable to platform_driver_register\n", __func__);
	}

	return ret;
}
static void __exit ssd1289_exit(void)
{
	pr_debug("%s\n", __func__);

	spi_unregister_driver(&spi_ssd1289_driver);
}

module_init(ssd1289_init);
module_exit(ssd1289_exit);

MODULE_DESCRIPTION("SSD1289 LCD Driver");
MODULE_AUTHOR("Andrej Strancar <info@hwlevel.com>");
MODULE_LICENSE("GPL");
