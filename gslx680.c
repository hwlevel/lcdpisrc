/* drivers/input/touchscreen/ft5x06_ts.c
 *
 * Silead gslx TouchScreen driver.
 *
 * Copyright (c) 2010  Focal tech Ltd.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <mach/irqs.h>
#include <linux/kernel.h>
#include <linux/semaphore.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/syscalls.h>
#include <linux/unistd.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/moduleparam.h>

//gslx680 firmware
#include "gslx680_fw.h"

//#include "gslx680.h"
#define GSLX680_MAX_TOUCH_POINTS	2

#define GSLX680_PRESS_MAX	0xFF
#define GSLX680_PRESS		0x7F

#define GSL_DATA_REG		0x80
#define GSL_STATUS_REG		0xe0
#define GSL_PAGE_REG		0xf0

#define GSLX680_NAME   "gslx680"

struct gslx680_platform_data {
    unsigned int irq_pin;
    unsigned int reset_pin;
    u8 rotate_codes[4];
};

struct ts_event {
	u16 x;
	u16 y;
	u8 event; /* 0 -- down; 1-- contact; 2 -- contact */
	u8 id;
	u16 pressure;
};

/* ts_event values */
#define CTP_POINT_UP		0x01
#define CTP_POINT_DOWN		0x00
#define CTP_POINT_CONTACT	0x02

struct gslx680_ts_data {
	unsigned int irq;
	unsigned int x_max;
	unsigned int y_max;
	struct i2c_client *client;
	struct input_dev *input_dev;
	struct ts_event events[GSLX680_MAX_TOUCH_POINTS];
	int touch_points;
	struct gslx680_platform_data *pdata;
};

/* module parameters */
unsigned int params = 0x80;
module_param(params, int, 0644);
MODULE_PARM_DESC(params, "Touch Panel Axes (s)wapping and (i)nverting. Last 3 bits: sXY iX iY");

/*
 * This struct is a touchpoint as stored in hardware.  Note that the id,
 * as well as the event, are stored in the upper nybble of the hi byte.
 */

static int gslx680_shutdown_low(struct gslx680_ts_data *ts) {
	gpio_set_value(ts->pdata->reset_pin, 0);
	return 0;
}

static int gslx680_shutdown_high(struct gslx680_ts_data *ts) {
	gpio_set_value(ts->pdata->reset_pin, 1);
	return 0;
}

static __inline__ void fw2buf(u8 *buf, const u32 *fw) {
	u32 *u32_buf = (u32 *)buf;
	*u32_buf = *fw;
}

static int gsl_ts_write(struct gslx680_ts_data *ts, u8 addr, u8 *pdata, int datalen) {
	if (datalen > 125) {
		return -1;
	}

	return i2c_smbus_write_i2c_block_data(ts->client, addr, datalen, pdata);
}

static int gsl_ts_read(struct gslx680_ts_data *ts, u8 addr, u8 *pdata, unsigned int datalen) {
	if (datalen > 126) {
		return -1;
	}

	return i2c_smbus_read_i2c_block_data(ts->client, addr, datalen, pdata);
}

static void reset_chip(struct gslx680_ts_data *ts) {

	u8 buf[1];

	buf[0]=0x88;
	gsl_ts_write(ts, GSL_STATUS_REG, buf, 1);
	mdelay(10);

	buf[0]=0x04;
	gsl_ts_write(ts, 0xe4, buf, 1);
	mdelay(10);

	buf[0]=0x00;
	gsl_ts_write(ts, 0xbc, buf, 1);
	mdelay(10);
	buf[0]=0x00;
	gsl_ts_write(ts, 0xbd, buf, 1);
	mdelay(10);
	buf[0]=0x00;
	gsl_ts_write(ts, 0xbe, buf, 1);
	mdelay(10);
	buf[0]=0x00;
	gsl_ts_write(ts, 0xbf, buf, 1);
	mdelay(10);

}

static void gsl_load_fw(struct gslx680_ts_data *ts) {

	u8 buf[9] = {0};
	int retval;
	int pos;

	u32 offset;
	u32 val;
	const unsigned char *buffer;

	printk("=============gsl_load_fw start==============\n");
	printk("Sending code\n");
	pos = 0;
	while(pos < gsl1680_fw_size) {
		buffer = &gsl1680_fw[pos];
		offset=((u32)buffer[0])+0x0100*((u32)buffer[1])+0x010000*((u32)buffer[2])+0x01000000*((u32)buffer[3]);
		val   =((u32)buffer[4])+0x0100*((u32)buffer[5])+0x010000*((u32)buffer[6])+0x01000000*((u32)buffer[7]);

		/* init page trans, set the page val */
		if (GSL_PAGE_REG == offset) {
			fw2buf(buf, &val);
			gsl_ts_write(ts, GSL_PAGE_REG, buf, 4);
		} else {
			buf[0] = (u8)offset;
			fw2buf(buf+1, &val);
			retval=gsl_ts_write(ts, buf[0], buf+1, 4);
			if(retval!=0) {
				printk("Error while sending firmware data\n");
			}
		}
		pos += 8;
	}

	printk("=============gsl_load_fw end==============\n");

}

static void startup_chip(struct gslx680_ts_data *ts) {
	u8 tmp = 0x00;
	gsl_ts_write(ts, GSL_STATUS_REG, &tmp, 1);
	mdelay(10);
}

static void init_chip(struct gslx680_ts_data *ts) {

	reset_chip(ts);
	gsl_load_fw(ts);
	startup_chip(ts);
	reset_chip(ts);
	gslx680_shutdown_low(ts);
	mdelay(50);
	gslx680_shutdown_high(ts);
	mdelay(30);
	gslx680_shutdown_low(ts);
	mdelay(5);
	gslx680_shutdown_high(ts);
	mdelay(20);
	reset_chip(ts);
	startup_chip(ts);
}

static int gslx680_read_touchdata(struct gslx680_ts_data *ts) {

	struct ts_event *event = ts->events;

	u8 buffer[GSLX680_MAX_TOUCH_POINTS * 4];
	u16 x, y;
	int retval;
	int i;

	retval=gsl_ts_read(ts, GSL_DATA_REG, buffer, 1);
	if (retval < 0) {
			dev_err(&ts->client->dev, "%s read touchdata failed.\n",
				__func__);
			return retval;
		}

	dev_dbg(&ts->input_dev->dev, "detected %d touch events\n",
			buffer[0]);

	ts->touch_points = buffer[0];
	if (ts->touch_points > GSLX680_MAX_TOUCH_POINTS) {
		dev_err(&ts->input_dev->dev,
			"touchscreen reports %d points, %d are supported\n",
			ts->touch_points, GSLX680_MAX_TOUCH_POINTS);
		ts->touch_points = GSLX680_MAX_TOUCH_POINTS;
	}

	retval=gsl_ts_read(ts, 0x84, buffer, GSLX680_MAX_TOUCH_POINTS * 4);
	for (i = 0; i < GSLX680_MAX_TOUCH_POINTS; i++) {
		x = (((unsigned int)buffer[4 * i + 0])+256*((unsigned int)buffer[4 * i + 1]))&0x0FFF;
		y = (((unsigned int)buffer[4 * i + 2])+256*((unsigned int)buffer[4 * i + 3]))&0x0FFF;

//		x = (x - 8) * 89 / 10;
//		y = (y - 5) * 157 / 10;
		x = (x - 8) * (ts->x_max * 10) / 470 / 10;
		y = (y - 5) * (ts->y_max * 10) / 260 / 10;

		if(params & 0x4)
			swap(x, y);

		if(params & 0x2)
			x = ts->x_max - x;

		if(params & 0x1)
			y = ts->y_max - y;

//		printk("%d, %d\n", x, y);
		event[i].x = x;
		event[i].y = y;
//		event[i].event = buf.points[i].event >> 6;
		event[i].id = buffer[4 * i + 3] >> 4;
		event[i].pressure = GSLX680_PRESS;
	}

	return 0;
}

static void gslx680_report_values(struct gslx680_ts_data *ts)
{
	struct ts_event *event = ts->events;
	int i = 0;

	for (i = 0; i < ts->touch_points; i++) {
		input_report_abs(ts->input_dev, ABS_MT_POSITION_X,
				 event[i].x);
		input_report_abs(ts->input_dev, ABS_MT_POSITION_Y,
				 event[i].y);
		input_report_abs(ts->input_dev, ABS_MT_PRESSURE,
				 event[i].pressure);
		input_report_abs(ts->input_dev, ABS_MT_TRACKING_ID,
				 event[i].id);
		if (event[i].event == CTP_POINT_DOWN
		 || event[i].event == CTP_POINT_CONTACT)
			input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR,
					 event[i].pressure);
		else
			input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR,
					 0);
		input_mt_sync(ts->input_dev);
	}

	/* Legacy touchscreen emulation */
	if (ts->touch_points > 0) {
		input_report_abs(ts->input_dev, ABS_X, event[0].x);
		input_report_abs(ts->input_dev, ABS_Y, event[0].y);
		input_report_abs(ts->input_dev, ABS_PRESSURE, event[0].pressure);
		input_report_key(ts->input_dev, BTN_TOUCH, 1);
		input_sync(ts->input_dev);
	}

	else if (ts->touch_points == 0) {
		input_report_abs(ts->input_dev, ABS_MT_TOUCH_MAJOR, 0);
		input_report_abs(ts->input_dev, ABS_PRESSURE, 0);
		input_report_key(ts->input_dev, BTN_TOUCH, 0);
		input_sync(ts->input_dev);
	}
}

static irqreturn_t gslx680_ts_interrupt(int irq, void *dev_id)
{
	struct gslx680_ts_data *gslx680_ts = dev_id;
	int ret = 0;

	// printk("irq\n");
	ret = gslx680_read_touchdata(gslx680_ts);
	if (ret == 0)
		gslx680_report_values(gslx680_ts);

	return IRQ_HANDLED;
}

static u32 gslx680_of_value(struct device_node *node, const char *propname)
{
    u32 val = -1;

    of_property_read_u32(node, propname, &val);
    return val;
}

#ifdef CONFIG_OF
static int gslx680_i2c_ts_probe_dt(struct device *dev,
                struct gslx680_platform_data *pdata)
{
    struct device_node *np = dev->of_node;
    u32 dummy;

    pdata->reset_pin = of_get_named_gpio(np, "reset-gpios", 0);
    pdata->irq_pin = of_get_named_gpio(np, "irq-gpios", 0);

    of_property_read_u8_array(np, "rotate-codes", pdata->rotate_codes, 4);

    dummy = gslx680_of_value(np, "rotate");
    if(dummy != -1) {
        if(dummy > 270)
            dummy = 270;

        params = pdata->rotate_codes[dummy / 90];
    }

    return 0;
}
#else
static inline int gslx680_i2c_ts_probe_dt(struct device *dev,
                    struct edt_ft5x06_ts_data *tsdata)
{
    return -ENODEV;
}
#endif

static int gslx680_ts_probe(struct i2c_client *client,
			   const struct i2c_device_id *id)
{
	struct gslx680_platform_data *pdata =
		(struct gslx680_platform_data *)client->dev.platform_data;
	struct gslx680_ts_data *gslx680_ts;
	struct input_dev *input_dev;
	int err = 0;
	u8 *tp;

    if (!pdata) {
        pdata = devm_kzalloc(&client->dev, sizeof(*pdata), GFP_KERNEL);
        if (!pdata) {
            dev_err(&client->dev, "failed to allocate driver data.\n");
            return -ENOMEM;
        }

        err = gslx680_i2c_ts_probe_dt(&client->dev, pdata);
        if (err) {
            dev_err(&client->dev,
                "DT probe failed and no platform data present\n");
            return err;
        }
    }

	if(params & 0x80) {
		tp = (u8*) kallsyms_lookup_name("lcdpi_touchparams");
		if(tp)
			params = *tp ^ 1;
	}

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		err = -ENODEV;
		goto exit_check_functionality_failed;
	}

	gslx680_ts = kzalloc(sizeof(struct gslx680_ts_data), GFP_KERNEL);

	if (!gslx680_ts) {
		err = -ENOMEM;
		goto exit_alloc_data_failed;
	}

	i2c_set_clientdata(client, gslx680_ts);
	gslx680_ts->client = client;
	gslx680_ts->pdata = pdata;
	gslx680_ts->x_max = 4095;
	gslx680_ts->y_max = 4095;

	if (gpio_is_valid(pdata->reset_pin)) {
		/* this pulls reset down, enabling the low active reset */
		err = gpio_request_one(pdata->reset_pin, GPIOF_OUT_INIT_LOW,
					 "gslx680 reset");
		if (err) {
			dev_err(&client->dev,
				"Failed to request GPIO %d as reset pin, error %d\n",
				pdata->reset_pin, err);
			return err;
		}
	}

	err = devm_gpio_request_one(&client->dev, pdata->irq_pin,
				GPIOF_DIR_IN, "gslx680 irq");
	if (err) {
		dev_err(&client->dev, "failed to request IRQ GPIO: %d\n", err);
		goto exit_request_reset;
	}

	gpio_set_value(pdata->reset_pin, 0);
	mdelay(100);
	gpio_set_value(pdata->reset_pin, 1);

	init_chip(gslx680_ts);

	gslx680_ts->irq = gpio_to_irq(pdata->irq_pin);

	err = devm_request_threaded_irq(&client->dev, gslx680_ts->irq,
					NULL, gslx680_ts_interrupt,
					IRQF_ONESHOT | IRQF_TRIGGER_RISING,
					client->dev.driver->name, gslx680_ts);
	if (err < 0) {
		dev_err(&client->dev, "%s: request irq failed\n", __func__);
		goto exit_irq_request_failed;
	}

	input_dev = input_allocate_device();
	if (!input_dev) {
		err = -ENOMEM;
		dev_err(&client->dev, "failed to allocate input device\n");
		goto exit_input_dev_alloc_failed;
	}

	gslx680_ts->input_dev = input_dev;

	input_dev->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	set_bit(ABS_MT_TOUCH_MAJOR, input_dev->absbit);
	set_bit(ABS_MT_POSITION_X, input_dev->absbit);
	set_bit(ABS_MT_POSITION_Y, input_dev->absbit);
	set_bit(ABS_MT_PRESSURE, input_dev->absbit);

	input_set_abs_params(input_dev,
			     ABS_MT_POSITION_X, 0, gslx680_ts->x_max, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_POSITION_Y, 0, gslx680_ts->y_max, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_TOUCH_MAJOR, 0, GSLX680_PRESS_MAX, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_PRESSURE, 0, GSLX680_PRESS_MAX, 0, 0);
	input_set_abs_params(input_dev,
			     ABS_MT_TRACKING_ID, 0, GSLX680_MAX_TOUCH_POINTS, 0, 0);
	input_set_abs_params(input_dev, ABS_X, 0, gslx680_ts->x_max, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, gslx680_ts->y_max, 0, 0);
	input_set_abs_params(input_dev, ABS_PRESSURE, 0x0, GSLX680_PRESS_MAX, 0, 0);

	set_bit(EV_KEY, input_dev->evbit);
	set_bit(EV_ABS, input_dev->evbit);

	input_dev->name = GSLX680_NAME;
	err = input_register_device(input_dev);
	if (err) {
		dev_err(&client->dev,
			"gslx680_ts_probe: failed to register input device: %s\n",
			dev_name(&client->dev));
		goto exit_input_register_device_failed;
	}

	/* allow touch panel controller to boot, before querying it */
	msleep(150);

	return 0;

exit_input_register_device_failed:
	input_free_device(input_dev);

exit_input_dev_alloc_failed:
exit_request_reset:
exit_irq_request_failed:
	i2c_set_clientdata(client, NULL);
	kfree(gslx680_ts);

exit_alloc_data_failed:
exit_check_functionality_failed:
	return err;
}

#ifdef CONFIG_PM
static int gslx680_ts_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);

	struct gslx680_ts_data *ts = i2c_get_clientdata(client);
	dev_dbg(&ts->client->dev, "gslx suspend\n");
	disable_irq(ts->pdata->irq_pin);
	return 0;
}

static int gslx680_ts_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);

	struct gslx680_ts_data *ts = i2c_get_clientdata(client);
	dev_dbg(&ts->client->dev, "gslx resume.\n");
	if (gpio_is_valid(ts->pdata->reset_pin)) {
		gpio_set_value(ts->pdata->reset_pin, 0);
		msleep(20);
		gpio_set_value(ts->pdata->reset_pin, 1);
	}
	enable_irq(ts->pdata->irq_pin);
	return 0;
}
#else
#define gslx680_ts_suspend	NULL
#define gslx680_ts_resume	NULL
#endif

static int gslx680_ts_remove(struct i2c_client *client)
{
	const struct gslx680_platform_data *pdata =
						dev_get_platdata(&client->dev);
	struct gslx680_ts_data *tsdata = i2c_get_clientdata(client);

	devm_free_irq(&client->dev, tsdata->irq, tsdata);
	input_unregister_device(tsdata->input_dev);

	if (gpio_is_valid(pdata->irq_pin))
		gpio_free(pdata->irq_pin);
	if (gpio_is_valid(pdata->reset_pin))
		gpio_free(pdata->reset_pin);

	kfree(tsdata);

	return 0;
}

static const struct i2c_device_id gslx680_ts_id[] = {
	{GSLX680_NAME, 0},
	{}
};

MODULE_DEVICE_TABLE(i2c, gslx680_ts_id);

static SIMPLE_DEV_PM_OPS(gslx680_ts_pm_ops,
			 gslx680_ts_suspend, gslx680_ts_resume);

static const struct of_device_id gslx680_dt_ids[] = {
    { .compatible = "silead,gslx680" },
    {},
};

MODULE_DEVICE_TABLE(of, gslx680_dt_ids);

static struct i2c_driver gslx680_ts_driver = {
	.probe = gslx680_ts_probe,
	.remove = gslx680_ts_remove,
	.id_table = gslx680_ts_id,
	.driver = {
		.name = GSLX680_NAME,
        .of_match_table = of_match_ptr(gslx680_dt_ids),
		.owner = THIS_MODULE,
		.pm = &gslx680_ts_pm_ops,

	},
};

static int __init gslx680_ts_init(void)
{
	int ret;
	ret = i2c_add_driver(&gslx680_ts_driver);
	if (ret)
		pr_err("Adding gslx driver failed (errno = %d)\n", ret);
	return ret;
}

static void __exit gslx680_ts_exit(void)
{
	i2c_del_driver(&gslx680_ts_driver);
}

module_init(gslx680_ts_init);
module_exit(gslx680_ts_exit);

MODULE_AUTHOR("Sean Cross <xobs@kosagi.com>");
MODULE_DESCRIPTION("Silead gslx680 TouchScreen driver");
MODULE_LICENSE("GPL");


