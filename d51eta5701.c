/*
 * Framebuffer driver for 480x320 Hwlevel display modules
 * based on D51ETA5701 driver chip
 * Copyright (c) 2015 Andrej Strancar
 *
 * Original: Copyright (c) 2009 Jean-Christian de Rivaz
 *
 * SPI mods, console support, 320x240 instead of 240x320:
 * Copyright (c) 2012 Jeroen Domburg <jeroen@spritesmods.com>
 *
 * Bits and pieces borrowed from the fsl-d51.c:
 * Copyright (C) 2010-2011 Freescale Semiconductor, Inc. All Rights Reserved.
 * Author: Alison Wang <b18965@freescale.com>
 *         Jason Jin <Jason.jin@freescale.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * The Solomon Systech SSD1289 chip drive TFT screen up to 240x320.
 *
 */

#define ROTATE 2
#define QUERY_GET_VARIABLES _IOR('K', 1, unsigned char *)

// #define DEBUG

#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/fb.h>
#include <asm/io.h>
#include <linux/spi/spi.h>
#include <linux/pinctrl/consumer.h>
#include <linux/dma-mapping.h>

#include <linux/version.h>
#include <linux/ioctl.h>
#include <asm/uaccess.h>

#include <linux/clk.h>
#include <linux/time.h>
#include <linux/delay.h>

/* module parameters */
static int compress = 1;
module_param(compress, int, 0444);
MODULE_PARM_DESC(compress, "SPI compresion 1/0");

static long sclk = 32000000;
module_param(sclk, long, 0444);
MODULE_PARM_DESC(sclk, "SPI clock frequency");

struct d51_page {
	unsigned short x;
	unsigned short y;
	unsigned short *buffer;
	unsigned short len;
	int must_update;
};

struct d51 {
	struct device *dev;
	spinlock_t spi_lock;
	struct spi_device *spidev;
	struct fb_info *info;
	unsigned int pages_count;
	struct d51_page *pages;
	unsigned long pseudo_palette[17];
	int backlight;
};



static int d51_spi_write(struct d51 *item, unsigned short value,
		unsigned int isdata);

static int d51_backlight_update_status(struct backlight_device *bd)
{
	struct d51 *item = bl_get_data(bd);


	if(bd->props.fb_blank == 0)
		item->backlight =  bd->props.brightness;
	else
		item->backlight = 0;

	// d51_spi_write(item, 0x0000, 0); //NOP
	item->pages[0].must_update=1;
	schedule_delayed_work(&item->info->deferred_work, 0);

	return 0;
}

static int d51_backlight_get_brightness(struct backlight_device *bd)
{
	return bd->props.brightness;
}

static void d51_unregister_backlight(struct d51 *item)
{
	const struct backlight_ops *bl_ops;

	if (item->info->bl_dev) {
		item->info->bl_dev->props.power = FB_BLANK_POWERDOWN;
		backlight_update_status(item->info->bl_dev);
		bl_ops = item->info->bl_dev->ops;
		backlight_device_unregister(item->info->bl_dev);
		item->info->bl_dev = NULL;
		kfree(bl_ops);
	}
}

static void d51_register_backlight(struct d51 *item)
{
	struct backlight_device *bd;
	struct backlight_properties bl_props = { 0, 1, };
	struct backlight_ops *bl_ops;

	bl_ops = kzalloc(sizeof(struct backlight_ops), GFP_KERNEL);
	if (!bl_ops) {
		dev_err(item->info->device,
				"%s: could not allocate memeory for backlight operations.\n",
				__func__);
		return;
	}

	bl_ops->get_brightness = d51_backlight_get_brightness;
	bl_ops->update_status = d51_backlight_update_status;
	bl_props.type = BACKLIGHT_RAW;
	/* Assume backlight is off, get polarity from current state of pin */
	bl_props.power = FB_BLANK_POWERDOWN;
	//        if (!gpio_get_value(info->gpio.led[0]))
	//                bl_props.state |= BL_CORE_DRIVER1;

	bd = backlight_device_register(dev_driver_string(item->info->device),
			item->info->device, item, bl_ops, &bl_props);
	if (IS_ERR(bd)) {
		dev_err(item->info->device,
				"cannot register backlight device (%ld)\n",
				PTR_ERR(bd));
		goto failed;
	}
	item->info->bl_dev = bd;

	//        if (!item->fbtftops.unregister_backlight)
	//                item->fbtftops.unregister_backlight = d51_unregister_backlight;

	item->info->bl_dev->props.brightness = 1;
	return;

	failed:
	kfree(bl_ops);
}

static struct spi_device *d51_spi_device;

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int d51_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg)
#else
static int d51_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg)
#endif
{
    unsigned char keys;

    switch (cmd)
    {
        case QUERY_GET_VARIABLES:
            spi_read(d51_spi_device, &keys, sizeof(unsigned char));
            keys = (keys >> 3) & 0x1f;
            if (copy_to_user((unsigned char *)arg, &keys, sizeof(unsigned char)))
            {
                return -EACCES;
            }
            break;

        default:
            return -EINVAL;
    }

    return 0;
}

#define BLOCKLEN 65536
static u8 *d51_spiblock;

#define MAX_DMA_PIXELS 32767
static struct {
	u8 *buf;
	dma_addr_t dma;
	size_t len;
} d51_spiblock_dma;

int write_spi_dma(struct d51 *item, size_t len)
{
        struct spi_transfer t = {
                .tx_buf = d51_spiblock_dma.buf,
                .len = len,
        };
        struct spi_message m;

        spi_message_init(&m);
        t.tx_dma = d51_spiblock_dma.dma;
        m.is_dma_mapped = 1;
        spi_message_add_tail(&t, &m);
        return spi_sync(item->spidev, &m);
}

/*
This routine will write a single 16-bit value, either as data or as a command
(depends on isdata). The LCD will clock this in because the SPIs /CS goes high.
*/

/*
 * bits in control byte
 */
#define PI35_LONG (1<<7)
#define PI35_BLOCK (1<<6)
#define PI35_RESET (1<<5)
#define PI35_RS (1<<4)
#define PI35_BL (1<<3)
#define PI35_RD (1<<0)
#define PI35_WR (1<<1)

static int d51_spi_write(struct d51 *item, unsigned short value,
				unsigned int isdata)
{

//    item->spidev->max_speed_hz=16000000;
//    item->spidev->master->setup(item->spidev);

//	item->backlight = 1;
	//item->backlight = item->info->bl_dev->props.brightness;

	/*
	 * control byte
	 */
	d51_spiblock[0]=((isdata?PI35_RS:0)|(item->backlight?PI35_BL:0))|PI35_RESET|PI35_RD;

	/*
	 * 16-bit data / command
	 */
	d51_spiblock[1]=(value>>8)&0xff;
	d51_spiblock[2]=(value)&0xff;

	spi_write(item->spidev, d51_spiblock, 3);
	return 0;
}

static inline void d51_reg_set(struct d51 *item, unsigned short reg,
				   unsigned short value)
{
	d51_spi_write(item, reg&0xff, 0);
	d51_spi_write(item, value, 1);
}

static inline void d51_data_block(struct d51 *item, unsigned short value,
				   unsigned short len)
{
	int i;

	d51_spiblock[0]=PI35_RS|(item->backlight?PI35_BL:0)|PI35_RESET|PI35_RD|PI35_BLOCK;
	d51_spiblock[1]=(value>>8)&0xff;
	d51_spiblock[2]=(value)&0xff;
	for(i=0; i<len/2; i++)
		d51_spiblock[i+3] = 0xcc;

	spi_write(item->spidev, d51_spiblock, i+3);
}

static void d51_update_all(struct d51 *item)
{
	unsigned short i;
	struct fb_deferred_io *fbdefio = item->info->fbdefio;
	for (i = 0; i < item->pages_count; i++) {
		item->pages[i].must_update=1;
	}
	schedule_delayed_work(&item->info->deferred_work, fbdefio->delay);
}

extern unsigned int compress_rgb(unsigned char *dest, unsigned int ptr, unsigned short *buffer, int blen, unsigned char compress_speedup);

static void d51_update(struct fb_info *info, struct list_head *pagelist) {
	struct d51 *item = (struct d51 *) info->par;
	struct page *page;
	int i;
	int ptr;
	unsigned short *buffer;
	int lasti, blen;
	unsigned short compress_speedup = 7;

	//struct timeval tv1, tv2;

	//We can be called because of pagefaults (mmap'ed framebuffer, pages
	//returned in *pagelist) or because of kernel activity
	//(pages[i]/must_update!=0). Add the former to the list of the latter.
	list_for_each_entry(page, pagelist, lru)
	{
		item->pages[page->index].must_update = 1;
	}

	//Copy changed pages.

	lasti = -2;
	ptr = 0;

	pr_debug("update starting\n");

	for (i = 0; i < item->pages_count; i++) {
		//ToDo: Small race here between checking and setting must_update,
		//maybe lock?
		if (item->pages[i].must_update) {
			item->pages[i].must_update = 0;

			buffer = item->pages[i].buffer;
			blen = item->pages[i].len;

			if(ptr >= MAX_DMA_PIXELS - item->pages[i].len) {
							pr_debug("Transfering block of max=%d bytes\n", ptr*4);
							lasti = -2;
						}

			if (i != lasti + 1) {
				if (ptr) {
					write_spi_dma(item, ptr*2+2);
					ptr = 0;
					pr_debug("Transfering block of %d bytes\n", ptr*4);
				}

				d51_reg_set(item, 0x0020, item->pages[i].y);
				d51_reg_set(item, 0x0021, item->pages[i].x);
				d51_spi_write(item, 0x0022, 0);
				pr_debug("Starting new block\n");

				/*
				 * control byte for long data transfer. Any number of 16-bit data can follow.
				 */
				d51_spiblock_dma.buf[0] = PI35_RS | (item->backlight ? PI35_BL : 0)
						| PI35_RESET | PI35_RD | PI35_LONG | PI35_BLOCK;
			}

            ptr = compress_rgb(d51_spiblock_dma.buf, ptr, buffer, blen, compress_speedup);
			lasti = i;
			pr_debug("Added page %d, ptr=%d\n", i, ptr);
		}
	}

	if (ptr) {
		pr_debug("Transfering block of %d bytes [%2x, %2x, %2x, ...]\n",
				ptr * 2 + 1, d51_spiblock[0], d51_spiblock[1],
				d51_spiblock[2]);
		//item->spidev->max_speed_hz=64000000;
		//item->spidev->master->setup(item->spidev);
		item->spidev->max_speed_hz=sclk;
		item->spidev->master->setup(item->spidev);

		/*
		 * long transfer. Control byte + ptr of 16-bit data
		 */
		d51_spiblock_dma.buf[(ptr * 2) + 1] = 0x00;
		write_spi_dma(item, ptr * 2 + 2);
		ptr = 0;
	}

}

static void d51_update_noc(struct fb_info *info, struct list_head *pagelist)
{
	struct d51 *item = (struct d51 *)info->par;
	struct page *page;
	int i;
	int x, ptr;
	unsigned short *buffer, value;
	unsigned short repeated, lasti;

	//struct timeval tv1, tv2;

	//We can be called because of pagefaults (mmap'ed framebuffer, pages
	//returned in *pagelist) or because of kernel activity
	//(pages[i]/must_update!=0). Add the former to the list of the latter.
	list_for_each_entry(page, pagelist, lru) {
		item->pages[page->index].must_update=1;
	}

	//Copy changed pages.

	lasti = -2;
	ptr = 0;
	repeated = 0;

	for (i=0; i<item->pages_count; i++) {
		//ToDo: Small race here between checking and setting must_update,
		//maybe lock?
		if (item->pages[i].must_update) {
			item->pages[i].must_update=0;

			buffer = item->pages[i].buffer;

			if(ptr >= MAX_DMA_PIXELS - item->pages[i].len) {
				pr_debug("Transfering block of max=%d bytes\n", ptr*4);
				lasti = -2;
			}

			if(i != lasti + 1) {
				if(ptr) {
					write_spi_dma(item, ptr*2+2);
					ptr = 0;
					pr_debug("Transfering block of %d bytes\n", ptr*4);
				}

				d51_reg_set(item, 0x0020, item->pages[i].y);
				d51_reg_set(item, 0x0021, item->pages[i].x);
				d51_spi_write(item, 0x0022, 0);
				pr_debug("Starting new block\n");

				/*
				 * control byte for long data transfer. Any number of 16-bit data can follow.
				 */
				d51_spiblock_dma.buf[0]=PI35_RS|(item->backlight?PI35_BL:0)|PI35_RESET|PI35_RD|PI35_LONG;
			}

			for (x=0; x<item->pages[i].len; x++) {
				value=buffer[x];
				d51_spiblock_dma.buf[(ptr*2)+1]=(value>>8)&0xff;
				d51_spiblock_dma.buf[(ptr*2)+2]=(value)&0xff;
				ptr++;
			}

			lasti = i;
			pr_debug("Added page %d, ptr=%d\n", i, ptr);
		}
	}

	if(ptr) {
		pr_debug("Transfering block of %d bytes [%2x, %2x, %2x, ...]\n", ptr*2+1, d51_spiblock[0],
						d51_spiblock[1], d51_spiblock[2]);
				item->spidev->max_speed_hz=sclk;
				item->spidev->master->setup(item->spidev);

				//do_gettimeofday(&tv1);

				/*
				 * long transfer. Control byte + ptr of 16-bit data
				 */
				write_spi_dma(item, ptr*2+2);

				//d51_spi_write(item, d51_REG_GDDRAM_DATA, 0);
				//spi_write(item->spidev, &d51_spiblock[1], ptr*2);
				//do_gettimeofday(&tv2);
				//pr_debug("Time ellapsed: %d us\n", tv2.tv_usec - tv1.tv_usec);
				ptr = 0;
	}

}

static void __init d51_setup(struct d51 *item)
{
	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	d51_spiblock[0]=PI35_RD;
	d51_spiblock[1]=0;
	d51_spiblock[2]=0;
	spi_write(item->spidev, d51_spiblock, 3);
	mdelay(50);

	d51_spiblock[0]=PI35_RESET|PI35_RD|PI35_RS;
	d51_spiblock[1]=0;
	d51_spiblock[2]=1;
	spi_write(item->spidev, d51_spiblock, 3);
	mdelay(50);

// 	OSCEN=1
//	d51_reg_set(item, 0X0000,0X0001);
//	mdelay(10);

	d51_reg_set(item, 0X0001,0X013c);
	d51_reg_set(item, 0X0002,0X0100);
	d51_reg_set(item, 0X0003,0X1030);

	d51_reg_set(item, 0X0004,0x000f);

	d51_reg_set(item, 0X0008,0X0808);
	d51_reg_set(item, 0X000A,0X0500);
	d51_reg_set(item, 0X000B,0X0000);
	d51_reg_set(item, 0X000C,0X0770);
	d51_reg_set(item, 0X000D,0X0000);
	d51_reg_set(item, 0X000E,0X0001);

	d51_reg_set(item, 0X0011,0X0406);
	d51_reg_set(item, 0X0012,0X000E);
	d51_reg_set(item, 0X0013,0X0222);
	d51_reg_set(item, 0X0014,0X001c);
	d51_reg_set(item, 0X0015,0X3679);
	d51_reg_set(item, 0X0016,0X0000);

	d51_reg_set(item, 0X0030,0X6A50);
	d51_reg_set(item, 0X0031,0X00C9);
	d51_reg_set(item, 0X0032,0XC7BE);
	d51_reg_set(item, 0X0033,0X0003);
	d51_reg_set(item, 0X0036,0X3443);
	d51_reg_set(item, 0X003B,0X0000);
	d51_reg_set(item, 0X003C,0X0000);

	d51_reg_set(item, 0X002C,0X6A50);
	d51_reg_set(item, 0X002D,0X00C9);
	d51_reg_set(item, 0X002E,0XC7BE);
	d51_reg_set(item, 0X002F,0X0003);
	d51_reg_set(item, 0X0035,0X3443);
	d51_reg_set(item, 0X0039,0X0000);
	d51_reg_set(item, 0X003A,0X0000);

	d51_reg_set(item, 0X0028,0X6A50);
	d51_reg_set(item, 0X0029,0X00C9);
	d51_reg_set(item, 0X002A,0XC7BE);
	d51_reg_set(item, 0X002B,0X0003);
	d51_reg_set(item, 0X0034,0X3443);
	d51_reg_set(item, 0X0037,0X0000);
	d51_reg_set(item, 0X0038,0X0000);

	mdelay(20);

	d51_reg_set(item, 0X0012,0X200E);

	mdelay(160);

	d51_reg_set(item, 0X0012,0X2003);

	mdelay(40);

	d51_reg_set(item, 0X0044,0X013F);
	d51_reg_set(item, 0X0045,0X0000);
	d51_reg_set(item, 0X0046,0X01df);
	d51_reg_set(item, 0X0047,0X0000);
	d51_reg_set(item, 0X0020,0X0000);
	d51_reg_set(item, 0X0021,0X0000);

	d51_reg_set(item, 0X0007,0X0012);

	mdelay(40);

	d51_reg_set(item, 0X0007,0X0017);
	mdelay(50);

	d51_spi_write(item, 0X0022, 0);
}

//This routine will allocate the buffer for the complete framebuffer. This
//is one continuous chunk of 16-bit pixel values; userspace programs
//will write here.
static int __init d51_video_alloc(struct d51 *item)
{
	unsigned int frame_size;

	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	frame_size = item->info->fix.line_length * item->info->var.yres;
	dev_dbg(item->dev, "%s: item=0x%p frame_size=%u\n",
		__func__, (void *)item, frame_size);

	item->pages_count = frame_size / PAGE_SIZE;
	if ((item->pages_count * PAGE_SIZE) < frame_size) {
		item->pages_count++;
	}
	dev_dbg(item->dev, "%s: item=0x%p pages_count=%u\n",
		__func__, (void *)item, item->pages_count);

	item->info->fix.smem_len = item->pages_count * PAGE_SIZE;
	item->info->fix.smem_start =
	    (unsigned long)vmalloc(item->info->fix.smem_len);
	if (!item->info->fix.smem_start) {
		dev_err(item->dev, "%s: unable to vmalloc\n", __func__);
		return -ENOMEM;
	}
	memset((void *)item->info->fix.smem_start, 0, item->info->fix.smem_len);

	return 0;
}

static void d51_video_free(struct d51 *item)
{
	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	// kfree((void *)item->info->fix.smem_start);
	vfree((void *)item->info->fix.smem_start);
}

//This routine will allocate a d51_page struct for each vm page in the
//main framebuffer memory. Each struct will contain a pointer to the page
//start, an x- and y-offset, and the length of the pagebuffer which is in the framebuffer.
static int __init d51_pages_alloc(struct d51 *item)
{
	unsigned short pixels_per_page;
	unsigned short yoffset_per_page;
	unsigned short xoffset_per_page;
	unsigned short index;
	unsigned short x = 0;
	unsigned short y = 0;
	unsigned short *buffer;
	unsigned int len;

	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	item->pages = kmalloc(item->pages_count * sizeof(struct d51_page),
			      GFP_KERNEL);
	if (!item->pages) {
		dev_err(item->dev, "%s: unable to kmalloc for d51_page\n",
			__func__);
		return -ENOMEM;
	}

	pixels_per_page = PAGE_SIZE / (item->info->var.bits_per_pixel / 8);
	yoffset_per_page = pixels_per_page / item->info->var.xres;
	xoffset_per_page = pixels_per_page -
	    (yoffset_per_page * item->info->var.xres);
	dev_dbg(item->dev, "%s: item=0x%p pixels_per_page=%hu "
		"yoffset_per_page=%hu xoffset_per_page=%hu\n",
		__func__, (void *)item, pixels_per_page,
		yoffset_per_page, xoffset_per_page);

	buffer = (unsigned short *)item->info->fix.smem_start;
	for (index = 0; index < item->pages_count; index++) {
		len = (item->info->var.xres * item->info->var.yres) -
		    (index * pixels_per_page);
		if (len > pixels_per_page) {
			len = pixels_per_page;
		}
		dev_dbg(item->dev,
			"%s: page[%d]: x=%3hu y=%3hu buffer=0x%p len=%3hu\n",
			__func__, index, x, y, buffer, len);
		item->pages[index].x = x;
		item->pages[index].y = y;
		item->pages[index].buffer = buffer;
		item->pages[index].len = len;

		x += xoffset_per_page;
		if (x >= item->info->var.xres) {
			y++;
			x -= item->info->var.xres;
		}
		y += yoffset_per_page;
		buffer += pixels_per_page;
	}

	return 0;
}

static void d51_pages_free(struct d51 *item)
{
	dev_dbg(item->dev, "%s: item=0x%p\n", __func__, (void *)item);

	kfree(item->pages);
	// vfree(item->pages);
}

static inline __u32 CNVT_TOHW(__u32 val, __u32 width)
{
	return ((val<<width) + 0x7FFF - val)>>16;
}

//This routine is needed because the console driver won't work without it.
static int d51_setcolreg(unsigned regno,
			       unsigned red, unsigned green, unsigned blue,
			       unsigned transp, struct fb_info *info)
{
	int ret = 1;

	/*
	 * If greyscale is true, then we convert the RGB value
	 * to greyscale no matter what visual we are using.
	 */
	if (info->var.grayscale)
		red = green = blue = (19595 * red + 38470 * green +
				      7471 * blue) >> 16;
	switch (info->fix.visual) {
	case FB_VISUAL_TRUECOLOR:
		if (regno < 16) {
			u32 *pal = info->pseudo_palette;
			u32 value;

			red = CNVT_TOHW(red, info->var.red.length);
			green = CNVT_TOHW(green, info->var.green.length);
			blue = CNVT_TOHW(blue, info->var.blue.length);
			transp = CNVT_TOHW(transp, info->var.transp.length);

			value = (red << info->var.red.offset) |
				(green << info->var.green.offset) |
				(blue << info->var.blue.offset) |
				(transp << info->var.transp.offset);

			pal[regno] = value;
			ret = 0;
		}
		break;
	case FB_VISUAL_STATIC_PSEUDOCOLOR:
	case FB_VISUAL_PSEUDOCOLOR:
		break;
	}
	return ret;
}

static int d51_blank(int blank_mode, struct fb_info *info)
{
	struct d51 *item = (struct d51 *)info->par;
	if (blank_mode == FB_BLANK_UNBLANK)
		item->backlight=1;
	else
		item->backlight=0;
	//Item->backlight won't take effect until the LCD is written to. Force that
	//by dirty'ing a page.
	item->pages[0].must_update=1;
	schedule_delayed_work(&info->deferred_work, 0);
	return 0;
}

static void d51_touch(struct fb_info *info, int x, int y, int w, int h)
{
	struct fb_deferred_io *fbdefio = info->fbdefio;
	struct d51 *item = (struct d51 *)info->par;
	int i, ystart, yend;
	if (fbdefio) {
		//Touch the pages the y-range hits, so the deferred io will update them.
		for (i=0; i<item->pages_count; i++) {
			ystart=item->pages[i].y;
			yend=item->pages[i].y+(item->pages[i].len/info->fix.line_length)+1;
			if (!((y+h)<ystart || y>yend)) {
				item->pages[i].must_update=1;
			}
		}
		//Schedule the deferred IO to kick in after a delay.
		schedule_delayed_work(&info->deferred_work, fbdefio->delay);
	}
}

static void d51_fillrect(struct fb_info *p, const struct fb_fillrect *rect)
{
	sys_fillrect(p, rect);
	d51_touch(p, rect->dx, rect->dy, rect->width, rect->height);
}

static void d51_imageblit(struct fb_info *p, const struct fb_image *image)
{
	sys_imageblit(p, image);
	d51_touch(p, image->dx, image->dy, image->width, image->height);
}

static void d51_copyarea(struct fb_info *p, const struct fb_copyarea *area)
{
	sys_copyarea(p, area);
	d51_touch(p, area->dx, area->dy, area->width, area->height);
}

static ssize_t d51_write(struct fb_info *p, const char __user *buf,
				size_t count, loff_t *ppos)
{
	ssize_t res;
	res = fb_sys_write(p, buf, count, ppos);
	d51_touch(p, 0, 0, p->var.xres, p->var.yres);
	return res;
}

static struct fb_ops d51_fbops = {
	.owner        = THIS_MODULE,
	.fb_read      = fb_sys_read,
	.fb_write     = d51_write,
	.fb_fillrect  = d51_fillrect,
	.fb_copyarea  = d51_copyarea,
	.fb_imageblit = d51_imageblit,
	.fb_setcolreg	= d51_setcolreg,
	.fb_blank	= d51_blank,
    .fb_ioctl = d51_ioctl
};

static struct fb_fix_screeninfo d51_fix __initdata = {
	.id          = "d51",
	.type        = FB_TYPE_PACKED_PIXELS,
	.visual      = FB_VISUAL_TRUECOLOR,
	.accel       = FB_ACCEL_NONE,
	.line_length = 320 * 2,
};

static struct fb_var_screeninfo d51_var __initdata = {
	.xres		= 320,
	.yres		= 480,
	.xres_virtual	= 320,
	.yres_virtual	= 480,
	.width		= 46,
	.height		= 70,
	.bits_per_pixel	= 16,
	.red		= {11, 5, 0},
	.green		= {5, 6, 0},
	.blue		= {0, 5, 0},
	.activate	= FB_ACTIVATE_NOW,
	.vmode		= FB_VMODE_NONINTERLACED,
};

static struct fb_deferred_io d51_defio = {
        .delay          = HZ / 20,
        .deferred_io    = &d51_update,
};

static int lcdpi_parse_dt(struct device *dev)
{
    struct device_node *node = dev->of_node;

    if(!node)
        return -1;

    // of_property_read_u8(node, "pi35,product-code", (u8*)&product_code);
    // of_property_read_u8(node, "pi35,invert-backlight", (u8*)&invbl);

    return 0;
}

static int __init d51_probe(struct spi_device *dev)
{
	int ret = 0;
	struct d51 *item;
	struct fb_info *info;

	dev_dbg(&dev->dev, "%s\n", __func__);

    lcdpi_parse_dt(&dev->dev);

	item = kzalloc(sizeof(struct d51), GFP_KERNEL);
	if (!item) {
		dev_err(&dev->dev,
			"%s: unable to kzalloc for d51\n", __func__);
		ret = -ENOMEM;
		goto out;
	}
	item->dev = &dev->dev;
	dev_set_drvdata(&dev->dev, item);
	item->backlight=0;

	d51_spi_device=item->spidev=dev;
	spin_lock_init(&item->spi_lock);
	item->dev=&dev->dev;
	spi_set_drvdata(dev, item);
	dev_info(&dev->dev, "spi registered, item=0x%p\n", (void *)item);

	d51_spiblock = (u8*)kmalloc(10, GFP_KERNEL);

    switch(compress) {
		case 0: /* 16-bit colors */
		d51_var.red.offset = 11;
		d51_var.red.length = 5;
		d51_var.green.offset = 5;
		d51_var.green.length = 6;
		break;

		case 1: /*15-bit colors */
		d51_var.red.offset = 10;
		d51_var.red.length = 5;
		d51_var.green.offset = 5;
		d51_var.green.length = 5;
		break;
	}

	info = framebuffer_alloc(sizeof(struct d51), &dev->dev);
	if (!info) {
		ret = -ENOMEM;
		dev_err(&dev->dev,
			"%s: unable to framebuffer_alloc\n", __func__);
		goto out_item;
	}
	info->pseudo_palette = &item->pseudo_palette;
	item->info = info;
	info->par = item;
	info->dev = &dev->dev;
	info->fbops = &d51_fbops;
	info->flags = FBINFO_FLAG_DEFAULT|FBINFO_VIRTFB;
	info->fix = d51_fix;
	info->var = d51_var;

	ret = d51_video_alloc(item);
	if (ret) {
		dev_err(&dev->dev,
			"%s: unable to d51_video_alloc\n", __func__);
		goto out_info;
	}
	info->screen_base = (char __iomem *)item->info->fix.smem_start;

	ret = d51_pages_alloc(item);
	if (ret < 0) {
		dev_err(&dev->dev,
			"%s: unable to d51_pages_init\n", __func__);
		goto out_video;
	}



	d51_register_backlight(item);

	switch(compress) {
			case 1:
			d51_defio.deferred_io = &d51_update;
			break;

			case 0:
			d51_defio.deferred_io = &d51_update_noc;
			break;
		}

	item->dev->coherent_dma_mask = ~0;
	//	d51_spiblock = (u8*)kmalloc(BLOCKLEN * 4, GFP_KERNEL);
	d51_spiblock_dma.buf = dma_alloc_coherent(item->dev, BLOCKLEN * 4, &d51_spiblock_dma.dma, GFP_DMA);
	d51_spiblock_dma.len = BLOCKLEN * 4;

	if(!d51_spiblock){
		dev_err(&dev->dev,
					"%s: unable to allocate spi buffer\n", __func__);
		goto out_pages;
	}

	info->fbdefio = &d51_defio;
	fb_deferred_io_init(info);

	if(!d51_spiblock){
		dev_err(&dev->dev,
					"%s: unable to allocate spi buffer\n", __func__);
		goto out_pages;
	}

	item->backlight = 1;
	d51_setup(item);
	d51_update_all(item);

	ret = register_framebuffer(info);
	if (ret < 0) {
		dev_err(&dev->dev,
			"%s: unable to register_frambuffer\n", __func__);
		goto out_pages;
	}

	return ret;

out_pages:
	d51_unregister_backlight(item);
	d51_pages_free(item);
out_video:
	d51_video_free(item);
out_info:
	framebuffer_release(info);
out_item:
	kfree(item);
	// vfree(item);
out:
	return ret;
}


// static int d51_remove(struct spi_device *spi)
// {
	// struct fb_info *info = dev_get_drvdata(&spi->dev);
	// struct d51 *item = (struct d51 *)info->par;
	// dev_set_drvdata(&spi->dev, NULL);
	// static int __devexit d51_remove(struct spi_device *dev)
	static int __exit d51_remove(struct spi_device *dev)
	// unregister_framebuffer(info);
	// d51_pages_free(item);
	// d51_video_free(item);
	// framebuffer_release(info);
	// kfree(item);
{
   struct d51 *item = dev_get_drvdata(&dev->dev);
   struct fb_info *info;

   dev_dbg(&dev->dev, "%s\n", __func__);

   dev_set_drvdata(&dev->dev, NULL);
   if (item) {
		d51_unregister_backlight(item);
        info = item->info;
        if (info) {
      	   fb_deferred_io_cleanup(info);
           unregister_framebuffer(info);
           framebuffer_release(info);
        }
        d51_pages_free(item);
        d51_video_free(item);

        if(d51_spiblock_dma.buf)
           dma_free_coherent(item->dev, BLOCKLEN * 4, d51_spiblock_dma.buf, d51_spiblock_dma.dma);

        kfree(item);

   }
   if(d51_spiblock)
	   kfree(d51_spiblock);
	return 0;
}

//#ifdef CONFIG_PM
//static int d51_suspend(struct spi_device *spi, pm_message_t state)
//{
//	struct fb_info *info = dev_get_drvdata(&spi->dev);
//	struct d51 *item = (struct d51 *)info->par;
//	/* enter into sleep mode */
//	d51_reg_set(item, d51_REG_SLEEP_MODE, 0x0001);
//	return 0;
//}
//
//static int d51_resume(struct spi_device *spi)
//{
//	struct fb_info *info = dev_get_drvdata(&spi->dev);
//	struct d51 *item = (struct d51 *)info->par;
//	/* leave sleep mode */
//	d51_reg_set(item, d51_REG_SLEEP_MODE, 0x0000);
//	return 0;
//}
//#else
#define d51_suspend NULL
#define d51_resume NULL
//#endif

    static const struct of_device_id lcdpi_dt_ids[] = {
        { .compatible = "hwlevel,pi35" },
        {},
    };

    MODULE_DEVICE_TABLE(of, lcdpi_dt_ids);

    static struct spi_driver spi_lcdpi_driver = {
        .driver = {
            .name	= "lcdpi-d51",
            .bus	= &spi_bus_type,
            .owner	= THIS_MODULE,
            .of_match_table = of_match_ptr(lcdpi_dt_ids),
        },

	.probe = d51_probe,
	.remove = d51_remove,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,1,0))
    .suspend = d51_suspend,
    .resume = d51_resume,
#endif
};

static int __init d51_init(void)
{
	int ret = 0;

    ret = spi_register_driver(&spi_lcdpi_driver);
	if (ret) {
		pr_err("%s: unable to platform_driver_register\n", __func__);
	}

	return ret;
}
static void __exit d51_exit(void)
{
       pr_debug("%s\n", __func__);

       spi_unregister_driver(&spi_lcdpi_driver);
}

module_init(d51_init);
module_exit(d51_exit);

MODULE_DESCRIPTION("D51ETA5710 LCD Driver");
MODULE_AUTHOR("Andrej Strancar <info@hwlevel.com>");
MODULE_LICENSE("GPL");
